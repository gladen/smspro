package com.tsadev.smspro.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.tsadev.smspro.R;


public class NewMessageFragment extends Fragment {

    View main;
    SearchView searchView;
    SearchableInfo searchInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Activity act = getActivity();
        if (act.getActionBar() != null) {
            act.getActionBar().setDisplayHomeAsUpEnabled(true);
            act.getActionBar().setTitle("New Message");
        }
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchInfo = searchManager.getSearchableInfo(getActivity().getComponentName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        if (main == null) {
            main = inflater.inflate(R.layout.fragment_new_msg, parent, false);
            searchView = (SearchView) main.findViewById(R.id.name_num);
            searchView.setSearchableInfo(searchInfo);
            searchView.setSubmitButtonEnabled(false);
        }
        return main;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public synchronized void intentReceived(Intent intent) {
        if (ContactsContract.Intents.SEARCH_SUGGESTION_CLICKED.equals(intent.getAction())) {
            //handles suggestion clicked query
            Contact contact = getContactFromIntent(intent);
            MessageListFragment mlf = new MessageListFragment();
            String threadId = String.valueOf(Telephony.Threads.getOrCreateThreadId(getContext(), contact.getNumber()));
            Bundle b = new Bundle();
            b.putString("name", contact.getDisplayName());
            b.putString("number", contact.getNumber());
            b.putString("thread_id", threadId);
            mlf.setArguments(b);
            getFragmentManager().beginTransaction()
                    .addToBackStack("new2view")
                    .replace(R.id.container, mlf, "view")
                    .commit();
        } else if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // handles a search query
            String query = intent.getStringExtra(SearchManager.QUERY);
        }
    }

    private Contact getContactFromIntent(Intent intent) {
        String name = "", number = "";
        ContentResolver cr = getActivity().getContentResolver();
        Cursor phoneCursor = cr.query(intent.getData(),
                new String[] {
                        ContactsContract.Contacts.DISPLAY_NAME,
                        ContactsContract.Contacts._ID
                }, null, null, null);
        if (phoneCursor != null && phoneCursor.moveToFirst()) {
            int nameIndx = phoneCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
            name = phoneCursor.getString(nameIndx);
            phoneCursor.close();
        }

        if (!name.isEmpty()) {
            phoneCursor = cr.query(
                    Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI, name),
                    new String[]{ ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER },
                    null, null, null);

            if (phoneCursor != null && phoneCursor.moveToFirst()) {
                int numIndx = phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER);
                number = phoneCursor.getString(numIndx);
                phoneCursor.close();
            }
        }
        if (!number.isEmpty()) {
            return new Contact(name, number);
        }
        else return new Contact("","");
    }

    private class Contact {
        private String displayName = "", number = "";

        Contact(String name, String number) {
            this.displayName = name;
            this.number = number;
        }

        String getDisplayName() {
            return this.displayName;
        }

        String getNumber() {
            return this.number;
        }

    }
}
