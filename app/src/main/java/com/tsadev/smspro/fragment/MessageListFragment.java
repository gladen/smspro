package com.tsadev.smspro.fragment;

import android.app.ActionBar;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.tsadev.smspro.object.MmsSmsItem;
import com.tsadev.smspro.widget.MessageAdapter;
import com.tsadev.smspro.service.MsgIntentSvc;
import com.tsadev.smspro.R;
import com.tsadev.smspro.util.Constants;

import java.util.ArrayList;

public class MessageListFragment extends Fragment {

    private static final int MENU_CALL = 10, MENU_SETTINGS = 20;

    View main;
    String name, number, thread_id;

    MsgBroadcastReciever rcv = new MsgBroadcastReciever();
    ArrayList<MmsSmsItem> listy = new ArrayList<>();
    MessageAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Bundle b = getArguments();
        if (b == null) {
            main = new TextView(getActivity());
            ((TextView)main).setText("Error");
            return;
        }
        //blah
        name = b.getString("name");
        number = b.getString("number");
        thread_id = b.getString("thread_id");

    }

    @Override
    public void onResume() {
        super.onResume();
        Intent i = new Intent(getContext(), MsgIntentSvc.class);
        i.setAction(Constants.ACTION_VIEW_THREAD);
        i.putExtra("thread_id", thread_id);
        getActivity().startService(i);
        ActionBar abar = getActivity().getActionBar();
        if (abar != null) {
            abar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white);
            abar.setDisplayHomeAsUpEnabled(true);
            abar.setTitle(name);
            abar.setSubtitle(number);
        }
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(rcv, new IntentFilter(Constants.ACTION_MESSAGES_LOADED));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(rcv);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        if (main == null) {
            main = inflater.inflate(R.layout.fragment_message_list, parent, false);
            final ListView msgList = (ListView) main.findViewById(R.id.msg_list);
            adapter = new MessageAdapter(getContext(), listy);
            msgList.setAdapter(adapter);
            adapter.registerDataSetObserver(new DataSetObserver() {
                @Override
                public void onInvalidated() {
                    super.onInvalidated();
                    msgList.smoothScrollToPosition(listy.size() - 1);
                }
            });

            ImageButton attach = (ImageButton) main.findViewById(R.id.message_attach),
                    send = (ImageButton) main.findViewById(R.id.message_send);
            final EditText messageBox = (EditText) main.findViewById(R.id.message_text);

            attach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String text = messageBox.getText().toString();
                    if (text.length() > 160) {
                        ArrayList<String> multiPartText = SmsManager.getDefault().divideMessage(text);
                        SmsManager.getDefault().sendMultipartTextMessage(number, null, multiPartText, null, null);
                    } else SmsManager.getDefault().sendTextMessage(number, null, text, null, null);
                    messageBox.setText("");
                    Intent i = new Intent(getContext(), MsgIntentSvc.class);
                    i.setAction(Constants.ACTION_VIEW_THREAD);
                    i.putExtra("thread_id", thread_id);
                    getActivity().startService(i);
                }
            });
        }
        return main;
    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add(0, MENU_CALL, 0, "Call").setIcon(R.drawable.ic_fa_phone).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.add(1, MENU_SETTINGS, 0, "Settings").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case MENU_CALL:
                Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
                startActivity(i);
                return true;
            case MENU_SETTINGS:
                return true;
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private class MsgBroadcastReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case Constants.ACTION_MESSAGES_LOADED:
                    Bundle b = intent.getExtras();
                    ArrayList<MmsSmsItem> list = b.getParcelableArrayList(Constants.MESSAGE_LIST_KEY);
                    assert list != null;
                    listy.clear();
                    listy.addAll(list);
                    adapter.notifyDataSetChanged();
                    break;
            }
        }
    }
}
