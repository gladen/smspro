package com.tsadev.smspro.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.tsadev.smspro.R;
import com.tsadev.smspro.service.MsgIntentSvc;
import com.tsadev.smspro.widget.ThreadAdapter;
import com.tsadev.smspro.object.ThreadItem;
import com.tsadev.smspro.util.Constants;

import java.util.ArrayList;
import java.util.List;


public class BaseFragment extends Fragment {

    private static final int MENU_SETTINGS = 10;

    Context con;
    View main;
    FloatingActionButton fab;
    List<ThreadItem> mList = new ArrayList<>();
    ThreadAdapter adapter;
    MsgBroadcastReciever rcv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rcv = new MsgBroadcastReciever();
        setHasOptionsMenu(true);
        con = getContext();
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent svcIntent = new Intent(getContext(), MsgIntentSvc.class);
        svcIntent.setAction(Constants.ACTION_LOAD_THREADS);
        getActivity().startService(svcIntent);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_THREADS_LOADED);
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(rcv, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(rcv);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (main == null) {
            main = inflater.inflate(R.layout.fragment_base, container, false);
            fab = (FloatingActionButton) main.findViewById(R.id.newMessage);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getFragmentManager().beginTransaction()
                            .addToBackStack("base2new")
                            .setCustomAnimations(android.R.animator.fade_in,
                                    android.R.animator.fade_out,
                                    android.R.animator.fade_in,
                                    android.R.animator.fade_out)
                            .replace(R.id.container, new NewMessageFragment(), "newmsg")
                            .commit();
                }
            });
            ListView threads = (ListView) main.findViewById(R.id.threads);
            adapter = new ThreadAdapter(con, mList);
            threads.setAdapter(adapter);
            threads.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ThreadItem tItem = adapter.getItem(position);
                    MessageListFragment mFrag = new MessageListFragment();
                    Bundle b = new Bundle();
                    b.putString("name", tItem.getDisplayName());
                    b.putString("number", tItem.getNormalNumber());
                    b.putString("thread_id", String.valueOf(tItem.getThreadId()));
                    mFrag.setArguments(b);
                    getFragmentManager().beginTransaction()
                            .addToBackStack("base2view")
                            .setCustomAnimations(android.R.animator.fade_in,
                                    android.R.animator.fade_out,
                                    android.R.animator.fade_in,
                                    android.R.animator.fade_out)
                            .replace(R.id.container, mFrag, "view")
                            .commit();
                }
            });
            threads.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    return false;
                }
            });
        }
        return main;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add(1, MENU_SETTINGS, 0, "Settings").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {

        switch (item.getItemId()) {
            case MENU_SETTINGS:
                FragmentManager fm = getFragmentManager();
                fm.beginTransaction()
                        .addToBackStack("base2settings")
                        .setCustomAnimations(android.R.animator.fade_in,
                                android.R.animator.fade_out,
                                android.R.animator.fade_in,
                                android.R.animator.fade_out)
                        .replace(R.id.container, new SettingsFragment(), "settings")
                        .commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class MsgBroadcastReciever extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case Constants.ACTION_THREADS_LOADED:
                    Bundle b = intent.getExtras();
                    ArrayList<ThreadItem> items = b.getParcelableArrayList(Constants.THREAD_LIST_KEY);
                    assert items != null;
                    mList.clear();
                    mList.addAll(items);
                    adapter.notifyDataSetChanged();
                    break;
            }
        }
    }
}
