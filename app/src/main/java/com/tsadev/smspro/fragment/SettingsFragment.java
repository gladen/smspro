package com.tsadev.smspro.fragment;

import android.content.Context;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.view.MenuItem;

public class SettingsFragment extends PreferenceFragment {

    Context con;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        con = getActivity();
        setHasOptionsMenu(true);
        getActivity().getActionBar().setTitle("Settings");
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        PreferenceScreen pScreen = getPreferenceManager().createPreferenceScreen(con);

        PreferenceCategory general = new PreferenceCategory(con);
        pScreen.addPreference(general);
        general.setTitle("General Settings");
        Preference dummy = new Preference(con);
        general.addPreference(dummy);
        dummy.setTitle("Dummy");
        dummy.setSummary("Dummy preference for testing purposes");
        setPreferenceScreen(pScreen);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
