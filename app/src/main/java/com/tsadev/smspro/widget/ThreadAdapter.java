package com.tsadev.smspro.widget;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tsadev.smspro.R;
import com.tsadev.smspro.object.ThreadItem;

import java.util.ArrayList;
import java.util.List;


public class ThreadAdapter extends BaseAdapter {

    private Context context;
    private ViewHolder holder;
    private List<ThreadItem> list = new ArrayList<>();

    @SuppressWarnings("unused")
    private ThreadAdapter() {
    }

    public ThreadAdapter(Context context, List<ThreadItem> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public ThreadItem getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getThreadId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.thread_list_item, parent, false);
            holder.contactPic = (ImageView) convertView.findViewById(R.id.thread_contact_pic);
            holder.contactName = (TextView) convertView.findViewById(R.id.thread_sender);
            holder.snippet = (TextView) convertView.findViewById(R.id.thread_snippet);
            holder.timestamp = (TextView) convertView.findViewById(R.id.thread_timestamp);
            convertView.setTag(holder);
        }
        else holder = (ViewHolder) convertView.getTag();
        ThreadItem item = list.get(position);
        byte[] photoBytes = item.getPhotoBytes();
        if (photoBytes != null && photoBytes.length > 0)
        holder.contactPic.setImageBitmap(BitmapFactory.decodeByteArray(photoBytes, 0, photoBytes.length));
        else holder.contactPic.setImageResource(R.drawable.stock_contact);
        holder.contactName.setText(item.getDisplayName());
        holder.snippet.setText(item.getSnippet());
        holder.timestamp.setText(item.getSimplifiedTime());

        return convertView;
    }

    private class ViewHolder {
        ImageView contactPic;
        TextView contactName, snippet, timestamp;
    }
}
