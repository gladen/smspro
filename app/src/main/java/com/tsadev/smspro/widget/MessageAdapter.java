package com.tsadev.smspro.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tsadev.smspro.R;
import com.tsadev.smspro.object.MmsItem;
import com.tsadev.smspro.object.MmsSmsItem;
import com.tsadev.smspro.object.SmsItem;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class MessageAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<MmsSmsItem> list = new ArrayList<>();
    private static final int SENT = 1001, RECEIVED = 1002;
    private boolean first = true;

    @SuppressWarnings("unused")
    private MessageAdapter() {
    }

    public MessageAdapter(Context context, ArrayList<MmsSmsItem> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public MmsSmsItem getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.message_list_item, parent, false);
            holder.start = convertView.findViewById(R.id.start);
            holder.end = convertView.findViewById(R.id.end);
            holder.messagePic = (ImageView) convertView.findViewById(R.id.message_picture);
            holder.body = (TextView) convertView.findViewById(R.id.message_body);
            holder.timestamp = (TextView) convertView.findViewById(R.id.message_timestamp);
            convertView.setTag(holder);
        }
        else holder = (ViewHolder) convertView.getTag();
        holder.start.setVisibility(View.GONE);
        holder.end.setVisibility(View.GONE);
        int pos = -1;
        MmsSmsItem item = list.get(position);
        if (!item.mms) {
            holder.messagePic.setImageBitmap(null);
            holder.messagePic.setVisibility(View.GONE);
            switch (item.type)  {
                case SmsItem.MESSAGE_TYPE_INBOX:
                    pos = RECEIVED;
                    break;
                case SmsItem.MESSAGE_TYPE_DRAFT:
                case SmsItem.MESSAGE_TYPE_FAILED:
                case SmsItem.MESSAGE_TYPE_QUEUED:
                case SmsItem.MESSAGE_TYPE_OUTBOX:
                case SmsItem.MESSAGE_TYPE_SENT:
                    pos = SENT;
                    break;
            }
            if (item.mText != null && !item.mText.isEmpty()) holder.body.setText(item.mText);
            else holder.body.setText(item.mText);
        } else {
            switch (item.mBox) {
                case MmsItem.MESSAGE_BOX_INBOX:
                    pos = RECEIVED;
                    break;
                case MmsItem.MESSAGE_BOX_DRAFTS:
                case MmsItem.MESSAGE_BOX_FAILED:
                case MmsItem.MESSAGE_BOX_SENT:
                case MmsItem.MESSAGE_BOX_OUTBOX:
                    pos = SENT;
                    break;
                default:
                    //err
                    break;
            }

            if (item.mText != null && !item.mText.isEmpty()) holder.body.setText(item.mText);
            else holder.body.setText("");
            ImageView[] views;
            if (item.mmsPartIds.size() > 1) {
                /*
                views = new ImageView[item.mmsPartIds.size() < 6 ? item.mmsPartIds.size() : 5];
                for (String s : item.mmsPartIds) {

                }
                for (ImageView view : views) {
                    view = new ImageView(context);
                }
                */
                ArrayList<String> list = new ArrayList<>();
                list.add(item.partId);
                getMmsPhotoBytesOnBackgroundThread(list, holder.messagePic);
            } else {
                getMmsPhotoBytesOnBackgroundThread(item.mmsPartIds, holder.messagePic);
            }

        }
        LinearLayout.LayoutParams bodyParams = (LinearLayout.LayoutParams)holder.body.getLayoutParams();
        LinearLayout.LayoutParams timeParams = (LinearLayout.LayoutParams)holder.timestamp.getLayoutParams();
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (5 * scale + 0.5f);
        holder.body.setPaddingRelative(pixels, pixels, pixels, pixels);
        switch (pos) {
            case SENT:
                holder.start.setVisibility(View.INVISIBLE);
                holder.body.setBackgroundResource(R.color.material_blue_grey_1);
                bodyParams.gravity = Gravity.END;
                timeParams.gravity = Gravity.END;
                holder.body.setLayoutParams(bodyParams);
                holder.timestamp.setLayoutParams(timeParams);
                holder.timestamp.setGravity(Gravity.END);
                break;
            case RECEIVED:
                holder.end.setVisibility(View.INVISIBLE);
                holder.body.setBackgroundResource(R.color.material_blue_2);
                bodyParams.gravity = Gravity.START;
                timeParams.gravity = Gravity.START;
                holder.body.setLayoutParams(bodyParams);
                holder.timestamp.setLayoutParams(timeParams);
                holder.timestamp.setGravity(Gravity.START);
                break;
        }
        holder.timestamp.setText(item.simpleTime);

        return convertView;
    }



    private void getMmsPhotoBytesOnBackgroundThread(final ArrayList<String> partIds, final ImageView... views) {

        new AsyncTask<Void, Void, byte[]>() {

            @Override
            protected byte[] doInBackground(Void... params) {
                byte[] photoBytes = null;
                if(partIds.size()==1 && views.length ==1) {
                    Uri partIDURI = Uri.parse("content://mms/part/" + partIds.get(0));
                    InputStream is;
                    try {
                        is = context.getContentResolver().openInputStream(partIDURI);
                        ByteArrayOutputStream os = new ByteArrayOutputStream();
                        byte[] buffer = new byte[0xFFF];
                        for (int len; (len = is.read(buffer)) != -1; ) os.write(buffer, 0, len);

                        os.flush();
                        photoBytes = os.toByteArray();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        //Do nothing
                    }

                }
                return photoBytes;
            }

            @Override
            protected void onPostExecute(byte[] result) {
                if (result != null && result.length > 0) {
                    views[0].setVisibility(View.VISIBLE);
                    Bitmap finishedBitmap = BitmapFactory.decodeByteArray(result, 0, result.length);
                    views[0].setImageBitmap(finishedBitmap);
                    if (first) {
                        first = false;
                        notifyDataSetInvalidated();
                    }
                } else {
                    views[0].setImageBitmap(null);
                    views[0].setVisibility(View.GONE);
                }
            }

        }.execute();

    }

    private class ViewHolder {
        ImageView messagePic;
        View start, end;
        TextView body, timestamp;
    }
}
