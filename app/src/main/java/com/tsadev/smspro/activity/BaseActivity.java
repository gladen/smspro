package com.tsadev.smspro.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentProvider;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;
import android.widget.Toolbar;

import com.tsadev.smspro.ContentProviderHelper;
import com.tsadev.smspro.SMSPro;
import com.tsadev.smspro.fragment.BaseFragment;
import com.tsadev.smspro.service.MsgIntentSvc;
import com.tsadev.smspro.fragment.NewMessageFragment;
import com.tsadev.smspro.R;
import com.tsadev.smspro.util.Constants;
import com.tsadev.smspro.util.Vars;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.ArrayList;


public class BaseActivity extends Activity {

    BaseFragment base;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ContentProviderHelper.getInstance(this).runQuery();
        SMSPro app = (SMSPro) getApplication();
        setContentView(R.layout.activity_base);
        Toolbar tbar = (Toolbar) findViewById(R.id.tbar);
        tbar.setTitle("Messages");
        setActionBar(tbar);
        getFragmentManager().addOnBackStackChangedListener(new BackStackListener());
        if (!needPermissions()) {
            FragmentManager fm = getFragmentManager();
            if (base == null) {
                base = new BaseFragment();
                fm.beginTransaction()
                        .add(R.id.container, base, "base")
                        .commit();
            }
        }
    }

    
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private boolean needPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList<String> perms = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
                perms.add(Manifest.permission.READ_SMS);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                perms.add(Manifest.permission.SEND_SMS);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_MMS) != PackageManager.PERMISSION_GRANTED) {
                perms.add(Manifest.permission.RECEIVE_MMS);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                perms.add(Manifest.permission.READ_CONTACTS);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                perms.add(Manifest.permission.WRITE_CONTACTS);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                perms.add(Manifest.permission.CALL_PHONE);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                perms.add(Manifest.permission.READ_PHONE_STATE);
            }
            if (perms.size() > 0) {
                Toast.makeText(this, "Permissions needed", Toast.LENGTH_LONG).show();
                String[] list = new String[perms.size()];
                int i = 0;
                for (String s : perms) {
                    list[i] = s;
                    i++;
                }
                ActivityCompat.requestPermissions(this, list, 10);
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int i = 0;
        Vars var = Vars.getInstance();
        for (String permission : permissions) {
            switch (permission) {
                case Manifest.permission.READ_SMS:
                    var.readSMS = (grantResults[i] == PackageManager.PERMISSION_GRANTED);
                    break;
                case Manifest.permission.SEND_SMS:
                    var.sendSMS = (grantResults[i] == PackageManager.PERMISSION_GRANTED);
                    break;
                case Manifest.permission.READ_CONTACTS:
                    var.readContacts = (grantResults[i] == PackageManager.PERMISSION_GRANTED);
                    break;
                case Manifest.permission.WRITE_CONTACTS:
                    var.writeContacts = (grantResults[i] == PackageManager.PERMISSION_GRANTED);
                    break;
                case Manifest.permission.CALL_PHONE:
                    var.call = (grantResults[i] == PackageManager.PERMISSION_GRANTED);
                default:
                    Log.d("TSDEBUG", "Permission requested: " + permission);
                    break;
            }
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                Log.d("TSDEBUG", "Permission denied: " + permission);
            }
            i++;
        }

        if (!var.readSMS|!var.sendSMS|!var.readContacts|!var.writeContacts) {
            Toast.makeText(this, "Permissions not granted, exiting...", Toast.LENGTH_SHORT).show();
            this.finish();
        }
        else {
            FragmentManager fm = getFragmentManager();
            if (base == null) {
                base = new BaseFragment();
                fm.beginTransaction()
                        .add(R.id.container, base, "base")
                        .commit();
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        switch (intent.getAction()) {
            case ContactsContract.Intents.SEARCH_SUGGESTION_CLICKED:
            case Intent.ACTION_SEARCH:
                Fragment cFrag = getFragmentManager().findFragmentById(R.id.container);
                if (cFrag != null && cFrag instanceof NewMessageFragment) {
                    NewMessageFragment nmFrag = (NewMessageFragment) cFrag;
                    nmFrag.intentReceived(intent);
                    return;
                } else {
                    super.onNewIntent(intent);
                    return;
                }
            default:
                super.onNewIntent(intent);
        }
    }

    private class BackStackListener implements FragmentManager.OnBackStackChangedListener {

        @Override
        public void onBackStackChanged() {
            int stackSize = getFragmentManager().getBackStackEntryCount();
            if (stackSize == 0) { //Back stack is empty
                try {
                    assert getActionBar() != null;
                    getActionBar().setTitle("Messages");
                    getActionBar().setSubtitle("");
                    getActionBar().setDisplayHomeAsUpEnabled(false);
                } catch (NullPointerException e) {
                    //this should never happen, so do nothing
                }
            }
        }
    }
}
