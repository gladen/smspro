package com.tsadev.smspro.old;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tsadev.smspro.R;
import com.tsadev.smspro.util.Constants;
import com.tsadev.smspro.util.Utils;
import com.tsadev.smspro.util.Vars;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final int MENU_SETTINGS = 10, MENU_DELETE = 110;
    private static final IntentFilter intentFilter = new IntentFilter();
    private final List<Conversation> messageList = new ArrayList<>();
    private ListView list;
    private ConversationListAdapter adapter;
    private LinearLayout root;
    private final BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        //
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || intent.getAction() == null) return;
            if (intent.getAction().equals(Constants.SMS_RECEIVED)) {
                messageList.clear();;
                adapter.notifyDataSetChanged();
            }
            else if (intent.getAction().equals(Constants.MMS_RECEIVED)) {
                Log.d("TSDEBUG", "MMS received");
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intentFilter.addAction(Constants.SMS_RECEIVED);
        intentFilter.addAction(Constants.MMS_RECEIVED);
        getAPN();
        setContentView(R.layout.activity_main);
        Vars.getInstance().getPreferences(this);
        list = (ListView)findViewById(R.id.message_list);
        final FloatingActionButton newMessage = (FloatingActionButton)findViewById(R.id.fab);
        newMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, NewMessageActivity.class);
                startActivity(i);
            }
        });
        boolean error = checkPermissions();
        if (error) {
            Toast.makeText(this, "Error, need permissions to run", Toast.LENGTH_SHORT).show();
            this.finish();
        }
        else {
            Vars.getInstance().defaultSms = Utils.checkIfDefaultSMS(this);
            if (!Vars.getInstance().defaultSms) {
                RelativeLayout convoRoot = (RelativeLayout) findViewById(R.id.main_root);
                root = new LinearLayout(this);
                root.setId(Utils.generateViewId());
                root.setOrientation(LinearLayout.VERTICAL);
                RelativeLayout.LayoutParams rootParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                rootParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
                root.setLayoutParams(rootParams);
                root.setBackgroundColor(0xDDbdbdbd);
                convoRoot.addView(root);
                RelativeLayout.LayoutParams listParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                listParams.addRule(RelativeLayout.BELOW, root.getId());
                list.setLayoutParams(listParams);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER;
                params.topMargin = params.bottomMargin = params.leftMargin = params.rightMargin = Utils.dpToPx(this, 5);

                TextView header = new TextView(this);
                header.setText("SMS Pro is not the default SMS app");
                header.setTypeface(null, Typeface.BOLD);
                header.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                header.setLayoutParams(params);
                root.addView(header);
                TextView secondary = new TextView(this);
                secondary.setText("In order to use full functionality of this app, please tap here to change default");
                secondary.setTypeface(null, Typeface.BOLD_ITALIC);
                secondary.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                secondary.setLayoutParams(params);
                root.addView(secondary);
                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.setDefaultSMS(MainActivity.this);
                    }
                });
            }
            final TelephonyManager manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            Vars.getInstance().myPhoneNumber = manager.getLine1Number();
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    final Conversation msg = (Conversation) parent.getItemAtPosition(position);
                    if (!msg.isSeen() && Vars.getInstance().defaultSms) {
                        ContentValues values = new ContentValues();
                        values.put("read", 1);
                        if (msg.isMMS())
                            getContentResolver().update(Telephony.Mms.CONTENT_URI, values, "_id=" + msg.getId(), null);
                        else
                            getContentResolver().update(Telephony.Sms.CONTENT_URI, values, "_id=" + msg.getId(), null);
                        msg.setSeen(true);
                        messageList.set(position, msg);
                        adapter.notifyDataSetChanged();
                    }

                    Intent intent = new Intent(MainActivity.this, MessageViewActivity.class);
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.putExtra("sender", msg.getSender());
                    intent.putExtra("thread_id", msg.getThreadId());

                    startActivity(intent);
                }
            });
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.cancelAll();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Utils.REQUEST_DEFAULT:
                if (Utils.checkIfDefaultSMS(this)) root.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(messageReceiver, intentFilter);
        registerForContextMenu(list);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterForContextMenu(list);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(messageReceiver);
        } catch (IllegalArgumentException e) {
            //Ignore
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, MENU_SETTINGS, Menu.NONE, "Settings");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_SETTINGS:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch (item.getItemId()) {
            case MENU_DELETE:
                deleteConversation(messageList.get(info.position).getThreadId());
                messageList.clear();
                adapter.notifyDataSetChanged();
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        if (v.getId()== list.getId()) {
            menu.setHeaderTitle("Message");
            menu.add(Menu.NONE, MENU_DELETE, 0, "Delete Message");
        }
    }

    private void deleteConversation(long thread_id) {
        int result = getContentResolver()
                .delete(Telephony.Threads.CONTENT_URI,
                        Telephony.Sms.THREAD_ID + "=" + thread_id, null);

        Log.d("TSDEBUG", "Deleted " + result + " row(s), thread: " + thread_id);
    }

    private boolean checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean tripwire = false;
            ArrayList<String> perms = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
                tripwire = true;
                perms.add(Manifest.permission.READ_SMS);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                tripwire = true;
                perms.add(Manifest.permission.SEND_SMS);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_MMS) != PackageManager.PERMISSION_GRANTED) {
                tripwire = true;
                perms.add(Manifest.permission.RECEIVE_MMS);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                tripwire = true;
                perms.add(Manifest.permission.READ_CONTACTS);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                tripwire = true;
                perms.add(Manifest.permission.WRITE_CONTACTS);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                tripwire = true;
                perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                tripwire = true;
                perms.add(Manifest.permission.READ_PHONE_STATE);
            }
            if (tripwire) {
                Toast.makeText(this, "Permissions needed", Toast.LENGTH_LONG).show();
                String[] list = new String[perms.size()];
                int i = 0;
                for (String s : perms) {
                    list[i] = s;
                    i++;
                }
                ActivityCompat.requestPermissions(this, list, 10);
                return true;
            }
            else {
                messageList.clear();
                adapter = new ConversationListAdapter(this, messageList);
                list.setAdapter(adapter);
                getSubscriptionId();
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int i = 0;
        for (String permission : permissions) {
            switch (permission) {
                case Manifest.permission.READ_SMS:
                    Vars.getInstance().readSMS = (grantResults[i] == PackageManager.PERMISSION_GRANTED);
                    adapter = new ConversationListAdapter(this, messageList);
                    list.setAdapter(adapter);
                    break;
                case Manifest.permission.SEND_SMS:
                    Vars.getInstance().sendSMS = (grantResults[i] == PackageManager.PERMISSION_GRANTED);
                    break;
                case Manifest.permission.READ_CONTACTS:
                    Vars.getInstance().readContacts = (grantResults[i] == PackageManager.PERMISSION_GRANTED);
                    break;
                case Manifest.permission.WRITE_CONTACTS:
                    Vars.getInstance().writeContacts = (grantResults[i] == PackageManager.PERMISSION_GRANTED);
                    break;
                default:
                    Log.d("TSDEBUG", "Permission requested: " + permission);
                    break;
            }
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                Log.d("TSDEBUG", "Permission denied: " + permission);
            }
            i++;
        }
    }

    @TargetApi(22)
    private void getSubscriptionId() {
        SubscriptionManager subMgr = SubscriptionManager.from(this);
        List<SubscriptionInfo> subInfo = subMgr.getActiveSubscriptionInfoList();
        if (subInfo.size() > 1) Utils.showSubSelector(this, subInfo);
        else Vars.getInstance().subscriptionId = SmsManager.getDefaultSmsSubscriptionId();
    }


    private String getAPN() {
        String apn = "";
        final Cursor apnCursor = getContentResolver()
                .query(Uri.withAppendedPath(Telephony.Carriers.CONTENT_URI, "current"),
                        APN_PROJECTION, null, null, null);
        if (apnCursor == null) return null;
        if (apnCursor.moveToFirst()) {
            int typeIndx = apnCursor.getColumnIndex(Telephony.Carriers.TYPE),
                    mmscIndx = apnCursor.getColumnIndex(Telephony.Carriers.MMSC),
                    proxyIndx = apnCursor.getColumnIndex(Telephony.Carriers.MMSPROXY),
                    portIndx = apnCursor.getColumnIndex(Telephony.Carriers.MMSPORT);
            do {
                String type = apnCursor.getString(typeIndx),
                        mmsc = apnCursor.getString(mmscIndx),
                        proxy = apnCursor.getString(proxyIndx),
                        port = apnCursor.getString(portIndx);
                if (!type.contains("mms") && mmsc.isEmpty() && proxy.isEmpty() && port.isEmpty()) continue;
                if (proxy.isEmpty() && port.isEmpty()) {
                    if (Vars.getInstance().mmsc.isEmpty()) Vars.getInstance().mmsc = mmsc;
                    Log.d("TSDEBUG", mmsc);
                }
            } while (apnCursor.moveToNext());
            apnCursor.close();
        }
        return apn;
    }

    private static final String[] APN_PROJECTION = {
            Telephony.Carriers.TYPE,
            Telephony.Carriers.MMSC,
            Telephony.Carriers.MMSPROXY,
            Telephony.Carriers.MMSPORT
    };

}
