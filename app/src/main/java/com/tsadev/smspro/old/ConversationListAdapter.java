package com.tsadev.smspro.old;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tsadev.smspro.R;
import com.tsadev.smspro.old.Conversation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class ConversationListAdapter extends BaseAdapter {
    private final List<Conversation> messageList;
    private final Context context;

    public ConversationListAdapter(Context context, List<Conversation> values) {
        this.context = context;
        this.messageList = values;

    }


    private String getMmsText(String id) {
        Uri partURI = Uri.parse("content://mms/part/" + id);
        InputStream is = null;
        StringBuilder sb = new StringBuilder();
        try {
            is = context.getContentResolver().openInputStream(partURI);
            if (is != null) {
                InputStreamReader isr = new InputStreamReader(is, "UTF-8");
                BufferedReader reader = new BufferedReader(isr);
                String temp = reader.readLine();
                while (temp != null) {
                    sb.append(temp);
                    temp = reader.readLine();
                }
            }
        } catch (IOException e) {
            //Nothing for now
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    //Nothing for now
                }
            }
        }
        return sb.toString();
    }

    @Override
    public int getCount() {
        return messageList.size();
    }

    @Override
    public Conversation getItem(int position) {
        return messageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return messageList.get(position).getThreadId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_row_item, parent, false);
            holder.picture = (ImageView)convertView.findViewById(R.id.contact_picture);
            holder.sender = (TextView)convertView.findViewById(R.id.message_sender);
            holder.message = (TextView)convertView.findViewById(R.id.message_short);
            holder.timestamp = (TextView)convertView.findViewById(R.id.timestamp);
            convertView.setTag(holder);
        }
        else holder = (Holder) convertView.getTag();
        Conversation convo = messageList.get(position);
        String sender;
        if (convo.getDisplayName().isEmpty()) sender = convo.getSender();
        else sender = convo.getDisplayName();
        String messageString = convo.getMessage();
        Date date = convo.getTime();
        String time;
        if (DateUtils.isToday(date.getTime())) time = new SimpleDateFormat("h:mm a", Locale.US).format(date);
        else time = new SimpleDateFormat("MMM d", Locale.US).format(date);
        holder.picture.setImageDrawable(null);
        Bitmap src = convo.getDisplayPicture();
        RoundedBitmapDrawable contactPic = RoundedBitmapDrawableFactory.create(context.getResources(), src);
        contactPic.setCornerRadius(Math.max(src.getWidth(), src.getHeight()) / 2.0f);
        holder.picture.setImageDrawable(contactPic);
        if (!convo.isSeen()) {
            holder.sender.setTypeface(null, Typeface.BOLD);
            holder.message.setTypeface(null, Typeface.BOLD);
            holder.timestamp.setTypeface(null, Typeface.BOLD);
        }
        else {
            holder.sender.setTypeface(null, Typeface.NORMAL);
            holder.message.setTypeface(null, Typeface.NORMAL);
            holder.timestamp.setTypeface(null, Typeface.NORMAL);
        }
        if (messageString.isEmpty()) {
            holder.message.setTypeface(null, Typeface.ITALIC);
            holder.message.setText("Picture");
        }
        else {
            holder.message.setTypeface(null, Typeface.NORMAL);
            holder.message.setText(messageString);
        }
        holder.sender.setText(sender);
        holder.timestamp.setText(time);

        return convertView;
    }

    private class Holder {
        public ImageView picture;
        public TextView sender, message, timestamp;
    }
}
