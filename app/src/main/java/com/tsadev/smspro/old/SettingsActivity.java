package com.tsadev.smspro.old;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.RingtonePreference;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.view.MenuItem;
import android.widget.Toast;

import com.tsadev.smspro.util.Utils;
import com.tsadev.smspro.util.Vars;

import java.util.List;

public class SettingsActivity extends AppCompatPreferenceActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final int REQUEST_PICTURE = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings");

        PreferenceScreen root = getPreferenceManager().createPreferenceScreen(this);
        setPreferenceScreen(root);

        PreferenceCategory notifications = new PreferenceCategory(this);
        notifications.setTitle("Notifications");
        root.addPreference(notifications);

        final CheckBoxPreference notifEnabled = new CheckBoxPreference(this);
        notifEnabled.setTitle("Enable Notifications");
        notifEnabled.setKey("notifications");
        notifEnabled.setDefaultValue(false);
        notifEnabled.setPersistent(true);
        notifications.addPreference(notifEnabled);

        final RingtonePreference notifRing = new RingtonePreference(this);
        notifRing.setKey("ringtonePath");
        notifRing.setTitle("Ringtone");
        String value = PreferenceManager.getDefaultSharedPreferences(this).getString("ringtonePath", null);
        if (value != null && !value.isEmpty()) {
            Uri ringtoneUri = Uri.parse(value);
            Ringtone ringtone = RingtoneManager.getRingtone(SettingsActivity.this, ringtoneUri);
            String name = ringtone.getTitle(SettingsActivity.this);
            notifRing.setSummary(name);
        }
        else notifRing.setSummary("Set the ringtone that will play for incoming messages");
        notifRing.setShowSilent(true);
        notifRing.setRingtoneType(RingtoneManager.TYPE_NOTIFICATION);
        notifRing.setPersistent(true);
        notifRing.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Uri ringtoneUri = Uri.parse((String) newValue);
                Ringtone ringtone = RingtoneManager.getRingtone(SettingsActivity.this, ringtoneUri);
                String name = ringtone.getTitle(SettingsActivity.this);
                notifRing.setSummary(name);
                return true;
            }
        });
        notifications.addPreference(notifRing);
        notifRing.setDependency(notifEnabled.getKey());

        final ListPreference notifVibrate = new ListPreference(this);
        notifVibrate.setTitle("Vibration Mode");
        notifVibrate.setKey("vibrateMode");
        notifVibrate.setEntries(new String[]{"Always", "While in vibrate only mode", "Never"});
        notifVibrate.setEntryValues(new String[]{"always", "vibrate_only", "never"});
        notifVibrate.setDefaultValue("vibrate_only");
        notifVibrate.setPersistent(true);
        notifVibrate.setSummary("%s");
        notifications.addPreference(notifVibrate);
        notifVibrate.setDependency(notifEnabled.getKey());

        PreferenceCategory messages = new PreferenceCategory(this);
        messages.setTitle("Messages");
        root.addPreference(messages);

        Preference background = new Preference(this);
        background.setTitle("Conversation Background");
        background.setSummary("Set the background image used in conversations");
        background.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (!checkPermissions()) return false;
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_PICTURE);
                return true;
            }
        });
        messages.addPreference(background);
        Preference clearBackground = new Preference(this);
        clearBackground.setTitle("Clear Conversation Background");
        clearBackground.setSummary("Clears the set conversation background");
        clearBackground.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Vars.getInstance().backgroundPath = "";
                PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this).edit().putString("backgroundPath", "").apply();
                Toast.makeText(SettingsActivity.this, "Background cleared", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        messages.addPreference(clearBackground);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager mgr = SubscriptionManager.from(this);
            final List<SubscriptionInfo> subInfo = mgr.getActiveSubscriptionInfoList();
            if (subInfo.size() > 1) {
                Preference subId = new Preference(this);
                subId.setTitle("SMS SIM");
                subId.setSummary("Select the SIM to send SMS messages with.");
                subId.setPersistent(false);
                subId.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        Utils.showSubSelector(SettingsActivity.this, subInfo);
                        return true;
                    }
                });
                messages.addPreference(subId);
            }
        }

        //PreferenceCategory debug = new PreferenceCategory(this);
        //debug.setTitle("Debug");
        //root.addPreference(debug);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int i = 0;
        for (String permission : permissions) {
            switch (permission) {
                case Manifest.permission.READ_EXTERNAL_STORAGE:
                    break;
                case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                    break;
                default:
                    break;
            }
            i++;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PICTURE && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            Cursor cursor = getContentResolver().query(selectedImage, null, null, null, null);
            if (cursor == null) return;
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                String picturePath = cursor.getString(columnIndex);
                if (picturePath != null) {
                    Vars.getInstance().backgroundPath = picturePath;
                    PreferenceManager.getDefaultSharedPreferences(this).edit().putString("backgroundPath", picturePath).apply();
                    Toast.makeText(this, "Background set", Toast.LENGTH_SHORT).show();
                }
                else Toast.makeText(this, "Could not set background, please try a different app", Toast.LENGTH_SHORT).show();
            }
            cursor.close();
        }
    }
}
