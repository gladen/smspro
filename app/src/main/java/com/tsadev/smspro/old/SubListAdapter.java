package com.tsadev.smspro.old;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.telephony.SubscriptionInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tsadev.smspro.R;

import java.util.List;

@TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
public class SubListAdapter extends BaseAdapter {
    private final List<SubscriptionInfo> subList;
    private final Context context;

    public SubListAdapter(Context context, List<SubscriptionInfo> values) {
        this.context = context;
        this.subList = values;

    }

    @Override
    public int getCount() {
        return subList.size();
    }

    @Override
    public SubscriptionInfo getItem(int position) {
        return subList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return subList.get(position).getSubscriptionId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_row_item, parent, false);
            convertView.setTag(holder);
        }
        else holder = (Holder) convertView.getTag();

        return convertView;
    }

    private class Holder {
        public TextView sender;
    }
}
