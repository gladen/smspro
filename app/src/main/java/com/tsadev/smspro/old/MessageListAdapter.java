package com.tsadev.smspro.old;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tsadev.smspro.R;
import com.tsadev.smspro.old.Message;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MessageListAdapter extends BaseAdapter {

    private List<Message> messageList = new ArrayList<>();
    private final Context context;

    public MessageListAdapter(Context context, List<Message> messages) {
        this.context = context;
        this.messageList = messages;
    }

    @Override
    public int getCount() {
        return messageList.size();
    }

    @Override
    public Message getItem(int position) {
        return messageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return messageList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Message msg = messageList.get(position);

        Holder holder;
        if (convertView != null && ((Holder)convertView.getTag()).type == msg.getType()) {
            holder = (Holder)convertView.getTag();
        }
        else {
            holder = new Holder();
            LayoutInflater inflater = LayoutInflater.from(context);
            holder.type = msg.getType();
            if (msg.getType() == 1) convertView = inflater.inflate(R.layout.convo_received, parent, false);
            else convertView = inflater.inflate(R.layout.convo_sent, parent, false);
            holder.mmsPicture = (ImageView)convertView.findViewById(R.id.convo_mms_picture);
            holder.message = (TextView)convertView.findViewById(R.id.convo_message);
            holder.timestamp = (TextView)convertView.findViewById(R.id.convo_timestamp);
            convertView.setTag(holder);
        }

        if (msg.isMMS()) {
            holder.message.setText("");
            msg.getMMS(context);
            if (msg.getContent() != null && !msg.getContent().isEmpty()) holder.message.setText(msg.getContent());
            Bitmap photo = msg.getMMSPicture();
            if (photo != null) {
                holder.mmsPicture.setVisibility(View.VISIBLE);
                holder.mmsPicture.setImageBitmap(photo);
                holder.mmsPicture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                                "/SMSPro";
                        File dir = new File(file_path);
                        if(!dir.exists())
                            dir.mkdirs();
                        File file = new File(dir, "IMG_" + msg.getTime() + msg.getExtension());
                        if (!file.exists()) {
                            FileOutputStream fOut;
                            try {
                                fOut = new FileOutputStream(file);
                                if (msg.getPhotoFormat() == Message.Format.PNG)
                                    msg.getFullSizeMMSPicture().compress(Bitmap.CompressFormat.PNG, 100, fOut);
                                else if (msg.getPhotoFormat() == Message.Format.JPEG)
                                    msg.getFullSizeMMSPicture().compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                                else msg.getFullSizeMMSPicture().compress(Bitmap.CompressFormat.PNG, 100, fOut);
                                fOut.flush();
                                fOut.close();
                            } catch (FileNotFoundException e) {
                                //Internal error, this should not occur
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        Intent viewPhoto = new Intent(Intent.ACTION_VIEW);
                        viewPhoto.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), "image/*");
                        context.startActivity(viewPhoto);
                    }
                });
            }
            else {
                Log.d("TSDEBUG", "Photo was null");
                holder.mmsPicture.setImageDrawable(null);
                holder.mmsPicture.setVisibility(View.GONE);
                holder.message.setText(msg.getContent());
            }
        }
        else {
            holder.mmsPicture.setImageDrawable(null);
            holder.mmsPicture.setVisibility(View.GONE);
            holder.message.setText(msg.getContent());
        }

        holder.timestamp.setText(msg.getFormattedTime());
        return convertView;
    }

    private class Holder {
        ImageView mmsPicture;
        TextView message, timestamp;
        int type;
    }
}
