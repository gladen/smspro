package com.tsadev.smspro.old;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tsadev.smspro.R;
import com.tsadev.smspro.old.graphics.drawable.BackgroundBitmapDrawable;
import com.tsadev.smspro.util.Constants;
import com.tsadev.smspro.util.ThreadUtils;
import com.tsadev.smspro.util.Vars;
import com.tsadev.smspro.view.AutoCompleteContactView;

import java.util.Date;

public class NewMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);
        final AutoCompleteContactView to = (AutoCompleteContactView)findViewById(R.id.to);
        final EditText message = (EditText)findViewById(R.id.new_message);
        ImageButton attach = (ImageButton)findViewById(R.id.new_attachment);
        if (getIntent() != null
                && getIntent().getStringExtra("to") != null) {
            to.setContactNumber(getIntent().getStringExtra("to"));
            to.setSelection(to.getText().length());
            if (getIntent().getStringExtra("sms_body") != null) {
                message.setText(getIntent().getStringExtra("sms_body"));
                message.setSelection(message.length());
            }
        }
        attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(NewMessageActivity.this, "Not implemented", Toast.LENGTH_SHORT).show();
            }
        });
        Button send = (Button)findViewById(R.id.new_send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String toPerson, toMessage;
                toPerson = to.getContactNumber();
                toMessage = message.getText().toString();
                message.setText("");
                boolean person = toPerson.isEmpty(), msg = toMessage.isEmpty();
                if (!person && !msg) {

                    TelephonyManager manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                    int phoneType = manager.getPhoneType();
                    SmsManager sms = SmsManager.getDefault();
                    if (phoneType == TelephonyManager.PHONE_TYPE_GSM) sms.sendTextMessage(toPerson, null, toMessage, null, null);
                    else if (phoneType == TelephonyManager.PHONE_TYPE_CDMA) {
                        sms.sendMultipartTextMessage(toPerson, null, sms.divideMessage(toMessage), null, null);
                    }
                    final long thread_id = ThreadUtils.getOrCreateThreadId(NewMessageActivity.this, toPerson);
                    if (Vars.getInstance().defaultSms) {
                        ContentValues values = new ContentValues();
                        values.put("address", toPerson);
                        values.put("body", toMessage);
                        values.put("thread_id", thread_id);
                        //values.put("status", 0);
                        values.put("date", new Date().getTime());
                        values.put("type", 2);
                        getContentResolver().insert(Telephony.Sms.Sent.CONTENT_URI, values);
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent openNewThread = new Intent();
                            openNewThread.setClass(NewMessageActivity.this, MessageViewActivity.class);
                            openNewThread.setAction(Intent.ACTION_VIEW);
                            openNewThread.putExtra("sender", toPerson);
                            openNewThread.putExtra("thread_id", String.valueOf(thread_id));
                            startActivity(openNewThread);
                            finish();
                        }
                    }, 500);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            sendBroadcast(new Intent(Constants.SMS_RECEIVED));
                        }
                    }, 3000);

                } else {
                    if (person && msg) {
                        Toast.makeText(NewMessageActivity.this, "Please fill both fields", Toast.LENGTH_SHORT).show();
                    } else if (person) {
                        Toast.makeText(NewMessageActivity.this, "Please enter a phone number", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(NewMessageActivity.this, "Please enter a message", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        if (!Vars.getInstance().backgroundPath.isEmpty()) {
            RelativeLayout root = (RelativeLayout)findViewById(R.id.new_background_image);
            BackgroundBitmapDrawable background = new BackgroundBitmapDrawable(getResources(), BitmapFactory.decodeFile(Vars.getInstance().backgroundPath));
            root.setBackground(background);
        }
    }
}
