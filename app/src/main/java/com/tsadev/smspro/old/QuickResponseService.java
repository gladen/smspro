package com.tsadev.smspro.old;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;

import com.tsadev.smspro.util.Constants;
import com.tsadev.smspro.util.ThreadUtils;
import com.tsadev.smspro.util.Vars;

import java.util.Date;

public class QuickResponseService extends IntentService {

    public QuickResponseService() {
        super("com.tsadev.smspro.service.QuickResponseService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null
                || !intent.getAction().equals(TelephonyManager.ACTION_RESPOND_VIA_MESSAGE)
                || intent.getData() == null) return;
        Uri in = intent.getData();
        String scheme = in.getScheme();
        String num = in.getEncodedSchemeSpecificPart()
                .replace("(", "")
                .replace(")", "")
                .replace("%20", "")
                .replace("-", "")
                .replace("%2B1", "");
        String body = intent.getStringExtra(Intent.EXTRA_TEXT);
        long thread_id = ThreadUtils.getOrCreateThreadId(this, num);

        TelephonyManager manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        int phoneType = manager.getPhoneType();
        SmsManager sms = SmsManager.getDefault();
        if (phoneType == TelephonyManager.PHONE_TYPE_GSM) sms.sendTextMessage(num, null, body, null, null);
        else if (phoneType == TelephonyManager.PHONE_TYPE_CDMA) {
            sms.sendMultipartTextMessage(num, null, sms.divideMessage(body), null, null);
        }
        if (Vars.getInstance().defaultSms) {
            final long time = new Date().getTime();
            ContentValues values = new ContentValues();
            values.put(Telephony.Sms.ADDRESS, Vars.getInstance().myPhoneNumber);
            values.put(Telephony.Sms.BODY, body);
            values.put(Telephony.Sms.THREAD_ID, thread_id);
            values.put(Telephony.Sms.STATUS, Telephony.Sms.STATUS_NONE);
            values.put(Telephony.Sms.DATE, time);
            values.put(Telephony.Sms.DATE_SENT,time);
            values.put(Telephony.Sms.TYPE, Telephony.Sms.MESSAGE_TYPE_SENT);
            values.put(Telephony.Sms.LOCKED, 0);
            values.put("cmasexpiretime", -1);
            values.put("cmas", 0);
            values.putNull("cmas_category");
            values.putNull("cmas_response");
            values.putNull("cmas_severity");
            values.putNull("cmas_urgency");
            values.putNull("cmas_certainty");
            values.put(Telephony.Sms.REPLY_PATH_PRESENT, 0);
            values.put("priority", 0);
            values.put(Telephony.Sms.PROTOCOL, 0);
            values.put(Telephony.Sms.SEEN, 1);
            values.put(Telephony.Sms.READ, 1);
            getContentResolver().insert(Telephony.Sms.Sent.CONTENT_URI, values);
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sendBroadcast(new Intent(Constants.SMS_RECEIVED));
            }
        }, 3000);
    }
}
