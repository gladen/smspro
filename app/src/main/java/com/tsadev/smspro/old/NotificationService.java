package com.tsadev.smspro.old;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;

import com.tsadev.smspro.R;
import com.tsadev.smspro.util.Vars;

public class NotificationService {

    private static final String PACKAGE_NAME = "com.tsadev.smspro";
    private static final int NOTIF_ID = 17590;

    public static void sendNotification(Context context, Intent intent, @Nullable Bitmap icon) {
        String displayName = intent.getStringExtra("displayName"),
                sender = intent.getStringExtra("sender"),
                message = intent.getStringExtra("message"), thread_id = intent.getStringExtra("thread_id");
        Intent i = new Intent(context, MessageViewActivity.class);
        i.setAction(Intent.ACTION_VIEW);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("sender", sender);
        i.putExtra("thread_id", thread_id);
        PendingIntent openMessageIntent = PendingIntent.getActivity(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder publicNotification = new Notification.Builder(context)
                .setGroup(PACKAGE_NAME)
                .setCategory(Notification.CATEGORY_MESSAGE)
                .setContentIntent(openMessageIntent)
                .setContentTitle(displayName)
                .setContentText("New Message Received")
                .setLargeIcon(icon)
                .setLocalOnly(true)
                .setNumber(0)
                .setOnlyAlertOnce(false)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSound(Uri.parse(Vars.getInstance().ringtonePath))
                .setAutoCancel(true);
        if (!Vars.getInstance().vibrateMode.isEmpty()) {
            String vMode = Vars.getInstance().vibrateMode;
            switch (vMode) {

                case "always":
                    publicNotification.setDefaults(Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE);
                    break;

                case "vibrate_only":
                    AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                    if (audio.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE)
                        publicNotification.setDefaults(Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE);
                    else publicNotification.setDefaults(Notification.DEFAULT_LIGHTS);
                    break;

                default:
                    publicNotification.setDefaults(Notification.DEFAULT_LIGHTS);
                    break;
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) publicNotification.setSmallIcon(Icon.createWithResource(context, R.drawable.smspro));


        Notification.Builder privateNotification = new Notification.Builder(context)
                .setGroup(PACKAGE_NAME)
                .setCategory(Notification.CATEGORY_MESSAGE)
                .setContentIntent(openMessageIntent) //Load into referenced conversation
                .setContentTitle(displayName)
                .setContentText(message)
                .setLargeIcon(icon)
                .setDefaults(Notification.DEFAULT_ALL)//Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE)
                .setLocalOnly(true)
                .setNumber(0) //Number of new messages
                .setPublicVersion(publicNotification.build()) //Obvious
                .setOnlyAlertOnce(false)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSound(Uri.parse(Vars.getInstance().ringtonePath))
                .setAutoCancel(true);
        if (!Vars.getInstance().vibrateMode.isEmpty()) {
            String vMode = Vars.getInstance().vibrateMode;
            switch (vMode) {

                case "always":
                    privateNotification.setDefaults(Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE);
                    break;

                case "vibrate_only":
                    AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                    if (audio.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE)
                        privateNotification.setDefaults(Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE);
                    else privateNotification.setDefaults(Notification.DEFAULT_LIGHTS);
                    break;

                default:
                    privateNotification.setDefaults(Notification.DEFAULT_LIGHTS);
                    break;
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) privateNotification.setSmallIcon(Icon.createWithResource(context, R.drawable.smspro));
        Notification notif = privateNotification.build();
        NotificationManager mgr = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        mgr.notify(NOTIF_ID, notif);
    }
}