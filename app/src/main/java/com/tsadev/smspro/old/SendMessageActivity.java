package com.tsadev.smspro.old;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Telephony;

import com.tsadev.smspro.util.ThreadUtils;

public class SendMessageActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() == null || getIntent().getAction() == null || getIntent().getData() == null) return;
        Uri in = getIntent().getData();
        String scheme = in.getScheme();
        String num = in.getEncodedSchemeSpecificPart()
                .replace("(", "")
                .replace(")", "")
                .replace("%20", "")
                .replace("-", "")
                .replace("%2B1", "");
        String msg = getIntent().getStringExtra("sms_body");
        if (msg == null) msg = "";
        Intent intent = new Intent();
        if (scheme.contains("sms")) intent.putExtra("messageType", "sms");
        else if (scheme.contains("mms")) intent.putExtra("messageType", "mms");
        else finish();
        long thread_id = ThreadUtils.getOrCreateThreadId(this, num);
        boolean newThread;

        ContentResolver cr = this.getContentResolver();
        Cursor cursor = cr.query(Uri.parse("content://mms-sms/conversations"),
                new String[]{Telephony.Sms.THREAD_ID}, Telephony.Sms.THREAD_ID + "=" + thread_id, null, null);
        if (cursor == null) return;
        newThread = !cursor.moveToFirst();
        cursor.close();

        if (newThread) {
            intent.setClass(this, NewMessageActivity.class);
            intent.putExtra("to", num);
            intent.putExtra("thread_id", String.valueOf(thread_id));
            intent.putExtra("sms_body", msg);
            startActivity(intent);
        }
        else {
            intent.setClass(this, MessageViewActivity.class);
            intent.setAction(Intent.ACTION_VIEW);
            intent.putExtra("sender", num);
            intent.putExtra("thread_id", String.valueOf(thread_id));
            intent.putExtra("sms_body", msg);
            startActivity(intent);
        }
        finish();
    }
}
