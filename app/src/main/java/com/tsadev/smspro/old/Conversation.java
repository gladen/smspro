package com.tsadev.smspro.old;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.annotation.Nullable;
import android.util.Log;

import com.tsadev.smspro.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Conversation {

    private int type;
    private boolean isMMS, seen;
    private long threadId, contactId;
    private String id = "", sender = "", message = "", displayName = "";
    private Date time;
    private Bitmap displayPicture;
    private Context context;

    private Conversation() {

    }

    public Conversation(Context context) {
        this.context = context;
    }

    public String getId() { return id; }

    public long getThreadId() { return threadId; }

    public String getSender() { return sender; }

    public String getMessage() { return message; }

    public boolean isSeen() { return seen; }

    public Date getTime() { return time; }

    public String getDisplayName() { return displayName; }

    public Bitmap getDisplayPicture() {
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
        if (cursor == null) return null;
        if (displayPicture == null) displayPicture = BitmapFactory.decodeResource(context.getResources(), R.drawable.stock_contact);
        try {
            if (cursor.moveToFirst()) {
                byte[] data = cursor.getBlob(0);
                if (data != null) {
                    Bitmap tmpPhoto = BitmapFactory.decodeByteArray(data, 0, data.length);
                    displayPicture = Bitmap.createScaledBitmap(tmpPhoto, 130, 130, true);
                }
            }
        } finally {
            cursor.close();
        }
        return displayPicture;
    }

    public boolean isMMS() { return isMMS; }

    public void setId(String id) { this.id = id; }

    public void setType(int type) { this.type = type; }

    public void setThreadId(int threadId) { this.threadId = threadId; }

    public void setContext(Context context) { this.context = context; }

    public void setSender(String sender) {
        if (sender != null && !sender.isEmpty()) {
            this.sender = sender;
            getDisplayItems();
        }
    }

    public void setMessage(String message) { this.message = message; }

    public void setSeen(boolean seen) { this.seen = seen; }

    public void setTime(Date time) { this.time = time; }

    public void setIsMMS(boolean isMMS) {
        this.isMMS = isMMS;
        if (isMMS) {
            setSender(getMMSAddress());
            setMessage(getMMSText());
        }
    }

    private String getMMSAddress() {
        String address = "";
        String selection;
        if (type == 1) { //Inbox
            selection = "type=137";
        }
        else if (type == 2) { //Sent
            selection = "type=151";
        }
        else return null;
        Uri uriAddrPart = Uri.parse("content://mms/" + id + "/addr");
        Cursor addrCur = context.getContentResolver()
                .query(uriAddrPart, null, selection, null, "_id");
        if (addrCur != null) {
            addrCur.moveToLast();
            int addColIndx = addrCur.getColumnIndex("address");
            address = addrCur.getString(addColIndx);
            addrCur.close();
        }
        return address;
    }

    private String getMMSText() {
        Uri partURI = Uri.parse("content://mms/part");
        Cursor cursor = context.getContentResolver().query(partURI, new String[]{
                Telephony.Mms.Part.TEXT,
                Telephony.Mms.Part.CONTENT_TYPE}, Telephony.Mms.Part.MSG_ID + "=?", new String[]{id}, null);
        if (cursor == null) return "";
        if (cursor.moveToFirst()) {
            int textIndx = cursor.getColumnIndex(Telephony.Mms.Part.TEXT),
                    ctntIndx = cursor.getColumnIndex(Telephony.Mms.Part.CONTENT_TYPE);
            if (cursor.getString(ctntIndx).equals("application/smil")) cursor.moveToNext();
            String text = cursor.getString(textIndx);
            if (text == null) {
                try {
                    cursor.moveToNext();
                    text = cursor.getString(textIndx);
                } catch (CursorIndexOutOfBoundsException e) {
                    return "";
                }
            }
            cursor.close();
            return text;
        }
        else return "";
    }

    private void getDisplayItems() {
        if (sender == null || sender.isEmpty()) return;
        displayPicture = BitmapFactory.decodeResource(context.getResources(), R.drawable.stock_contact);
        Uri personUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, sender);
        Cursor cur = context.getContentResolver().query(personUri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID},
                null, null, null);
        if (cur == null) return;
        if (cur.moveToFirst()) {
            int nameIdx = cur.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME);
            displayName = cur.getString(nameIdx);
            contactId = cur.getLong(cur.getColumnIndex(ContactsContract.PhoneLookup._ID));
            cur.close();
        }
    }
}
