package com.tsadev.smspro.old;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

import com.tsadev.smspro.util.Constants;
import com.tsadev.smspro.util.ThreadUtils;
import com.tsadev.smspro.util.Vars;

import java.util.Date;
import java.util.List;


public class ReceiverSms extends BroadcastReceiver {

    private final ContactInfo info = new ContactInfo();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Telephony.Sms.Intents.SMS_DELIVER_ACTION)) {
            final Bundle bundle = intent.getExtras();
            String address = "", message = "";
            try {
                if (bundle != null) {
                    final Object[] pdusObj = (Object[]) bundle.get("pdus");

                    for (Object currentObj : pdusObj) {
                        Intent messageIntent = new Intent();
                        SmsMessage currentMessage;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                            currentMessage = SmsMessage.createFromPdu((byte[]) currentObj, intent.getStringExtra("format"));
                        else //noinspection deprecation
                            currentMessage = SmsMessage.createFromPdu((byte[]) currentObj);
                        address = currentMessage.getDisplayOriginatingAddress();
                        message = currentMessage.getDisplayMessageBody();
                        int status = -1,
                                type = 1,
                                read = 0,
                                proto = currentMessage.getProtocolIdentifier();
                        boolean replyPathPresent = currentMessage.isReplyPathPresent();
                        long time = currentMessage.getTimestampMillis(),
                                thread_id = ThreadUtils.getOrCreateThreadId(context, address);
                        getDisplayNameAndPicture(context, address);
                        String sender = "";
                        if (info.displayName.isEmpty()) {
                            String tmpNum = currentMessage.getDisplayOriginatingAddress()
                                    .replace("(", "")
                                    .replace(")", "")
                                    .replace("-", "")
                                    .replace(" ", "")
                                    .replace("+1", "");
                            if (tmpNum.length() == 10) {
                                sender = "(" + tmpNum.substring(0, 3) + ") " + tmpNum.substring(3, 6) + "-" + tmpNum.substring(6, 10);
                            }
                            messageIntent.putExtra("displayName", sender);
                        } else messageIntent.putExtra("displayName", info.displayName);

                        messageIntent.putExtra("sender", address);
                        messageIntent.putExtra("message", message);
                        messageIntent.putExtra("thread_id", thread_id);

                        if (Vars.getInstance().defaultSms) {
                            ContentValues values = new ContentValues();
                            values.put(Telephony.Sms.ADDRESS, address);
                            values.put(Telephony.Sms.BODY, message);
                            values.put(Telephony.Sms.THREAD_ID, thread_id);
                            values.put(Telephony.Sms.STATUS, status);
                            values.put(Telephony.Sms.LOCKED, 0);
                            values.put("cmasexpiretime", -1);
                            values.put("cmas", 0);
                            values.putNull("cmas_category");
                            values.putNull("cmas_response");
                            values.putNull("cmas_severity");
                            values.putNull("cmas_urgency");
                            values.putNull("cmas_certainty");
                            values.put(Telephony.Sms.REPLY_PATH_PRESENT, replyPathPresent);
                            values.put("priority", 0);
                            values.put(Telephony.Sms.PROTOCOL, proto);
                            values.put(Telephony.Sms.SEEN, read);
                            values.put(Telephony.Sms.READ, read);
                            values.put(Telephony.Sms.DATE, new Date().getTime());
                            values.put(Telephony.Sms.DATE_SENT, time);
                            values.put(Telephony.Sms.TYPE, type);
                            context.getContentResolver().insert(Telephony.Sms.Inbox.CONTENT_URI, values);
                        }

                        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                        List<ActivityManager.RunningTaskInfo> tasks = manager.getRunningTasks(1);
                        String topActivityName = tasks.get(0).topActivity.getPackageName();
                        if (!(topActivityName.equalsIgnoreCase("com.tsadev.smspro"))) {
                            if (Vars.getInstance().notifications)
                                NotificationService.sendNotification(context, messageIntent, info.picture);
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("TSDEBUG", "SMS Exception: " + e);
            }
            context.sendBroadcast(new Intent(Constants.SMS_RECEIVED));
        }
        else if (intent.getAction().equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION))
            context.sendBroadcast(new Intent(Constants.SMS_RECEIVED));
        else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Log.d("TSDEBUG", "Boot completed intent received");
        }
    }


    public void getDisplayNameAndPicture(Context context, String phoneNumber) {
            Uri personUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                    phoneNumber.replace("(", "")
                            .replace(")", "")
                            .replace("-", "")
                            .replace(" ", "")
                            .replace("+1", ""));
            Cursor cur = context.getContentResolver().query(personUri,
                    new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID }, null, null, null);
            if (cur == null) return;
            if( cur.moveToFirst() ) {
                info.displayName = cur.getString(cur.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                info.id = cur.getLong(cur.getColumnIndex(ContactsContract.PhoneLookup._ID));
                cur.close();
            }
            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, info.id);
            Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
            Cursor cursor = context.getContentResolver().query(photoUri,
                    new String[] {ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
            if (cursor == null) return;
            try {
                if (cursor.moveToFirst()) {
                    byte[] data = cursor.getBlob(0);
                    if (data != null) {
                        Bitmap tmpPic = BitmapFactory.decodeByteArray(data, 0, data.length);
                        info.picture = Bitmap.createScaledBitmap(tmpPic, 130, 130, true);
                    }
                }
            } finally {
                cursor.close();
            }
    }

    private class ContactInfo {
        public String displayName = "";
        public Bitmap picture;
        public long id;
    }
}
