package com.tsadev.smspro.old;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.tsadev.smspro.mms.ContentType;
import com.tsadev.smspro.mms.pdu.EncodedStringValue;
import com.tsadev.smspro.mms.pdu.GenericPdu;
import com.tsadev.smspro.mms.pdu.MultimediaMessagePdu;
import com.tsadev.smspro.mms.pdu.NotificationInd;
import com.tsadev.smspro.mms.pdu.PduBody;
import com.tsadev.smspro.mms.pdu.PduHeaders;
import com.tsadev.smspro.mms.pdu.PduParser;
import com.tsadev.smspro.mms.pdu.PduPart;
import com.tsadev.smspro.mms.pdu.PduPersister;
import com.tsadev.smspro.mms.transaction.HttpUtils;
import com.tsadev.smspro.mms.transaction.TransactionSettings;
import com.tsadev.smspro.util.Constants;
import com.tsadev.smspro.util.Utils;
import com.tsadev.smspro.util.Vars;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;

public class ReceiverMms extends BroadcastReceiver {

    /**
     * Return codes for <code>enableApnType()</code>
     */
    static final int APN_ALREADY_ACTIVE     = 0;
    static final int APN_REQUEST_STARTED    = 1;
    static final int APN_TYPE_NOT_AVAILABLE = 2;
    static final int APN_REQUEST_FAILED     = 3;
    static final int APN_ALREADY_INACTIVE   = 4;

    static final String FEATURE_ENABLE_MMS = "enableMMS";
    static final String REASON_VOICE_CALL_ENDED = "2GVoiceCallEnded";



    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getAction() == null) return;
        if (intent.getAction().equals(Telephony.Sms.Intents.WAP_PUSH_DELIVER_ACTION)) {
            Log.d("TSDEBUG", "Broadcast Received!");
            if (!Utils.checkIfDefaultSMS(context)) return;
            ConnectivityManager mConnMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            TransactionSettings transactionSettings
                    = new TransactionSettings(context, mConnMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE_MMS).getExtraInfo());
            if (beginMmsConnectivity(mConnMgr)) {
                try {
                    ensureRouteToHost(mConnMgr, Vars.getInstance().mmsc, transactionSettings);
                    byte[] rawPdu = HttpUtils.httpConnection(
                            context,
                            -1,
                            Vars.getInstance().mmsc, null,
                            HttpUtils.HTTP_GET_METHOD,
                            transactionSettings.isProxySet(),
                            transactionSettings.getProxyAddress(),
                            transactionSettings.getProxyPort());
                    MultimediaMessagePdu mmsPdu = (MultimediaMessagePdu) new PduParser(rawPdu).parse();
                    TelephonyManager mgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                    getRecipients(mmsPdu, mgr);
                    processPduAttachments(mmsPdu);
                    PduPersister.getPduPersister(context).persist(mmsPdu, Telephony.Mms.Inbox.CONTENT_URI, true, false, null);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Log.d("TSDEBUG", "Couldn't begin connectivity");

                final Bundle bundle = intent.getExtras();
                String location = "";
                if (bundle != null) {
                    // header, contentTypeParameters, pduType, data, slot, phone, subscription, transactionId
                    NotificationInd ind = (NotificationInd) new PduParser((byte[]) bundle.get("data")).parse();
                    location = new String(ind.getContentLocation());
                }
                Intent rIntent = new Intent(Constants.MMS_RECEIVED);
                rIntent.setClass(context.getApplicationContext(), MainActivity.class);
                PendingIntent pI = PendingIntent.getBroadcast(context, 0, rIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    SmsManager.getSmsManagerForSubscriptionId(Vars.getInstance().subscriptionId)
                            .downloadMultimediaMessage(context.getApplicationContext(),
                                    location,
                                    Telephony.Mms.Inbox.CONTENT_URI,
                                    null,
                                    pI);
                }
            }
        }
        else if (intent.getAction().equals(Telephony.Sms.Intents.WAP_PUSH_RECEIVED_ACTION)) {
            Log.d("TSDEBUG", "MMS received intent");
        }
    }

    private boolean beginMmsConnectivity(ConnectivityManager mConnMgr) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                NetworkRequest request = new NetworkRequest.Builder()
                        .addTransportType(ConnectivityManager.TYPE_MOBILE)
                        .addCapability(NetworkCapabilities.NET_CAPABILITY_MMS)
                        .build();
                mConnMgr.requestNetwork(request, (ConnectivityManager.NetworkCallback)null);
                Network[] networks = mConnMgr.getAllNetworks();
                NetworkInfo info = null;
                for (int i = 0; i > networks.length; i++) {
                     NetworkInfo tempInfo = mConnMgr.getNetworkInfo(networks[i]);
                    if (tempInfo.getType() == ConnectivityManager.TYPE_MOBILE) info = tempInfo;
                }
                return info != null && info.isConnected();
            } catch (Exception e) {
                return false;
            }
        }
        else {
            try {
                int result = mConnMgr.startUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, FEATURE_ENABLE_MMS);
                NetworkInfo info = mConnMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE_MMS);
                return info != null
                        && info.isConnected()
                        && result == APN_ALREADY_ACTIVE
                        && !REASON_VOICE_CALL_ENDED.equals(info.getReason());
            } catch (Exception e) {
                return false;
            }
        }
    }


    private static void ensureRouteToHost(ConnectivityManager cm, String url, TransactionSettings settings) throws IOException {
        int inetAddr;
        if (settings.isProxySet()) {
            String proxyAddr = settings.getProxyAddress();
            inetAddr = lookupHost(proxyAddr);
            if (inetAddr == -1) {
                throw new IOException("Cannot establish route for " + url + ": Unknown host");
            } else {
                if (!cm.requestRouteToHost(ConnectivityManager.TYPE_MOBILE_MMS, inetAddr))
                    throw new IOException("Cannot establish route to proxy " + inetAddr);
            }
        } else {
            Uri uri = Uri.parse(url);
            inetAddr = lookupHost(uri.getHost());
            if (inetAddr == -1) {
                throw new IOException("Cannot establish route for " + url + ": Unknown host");
            } else {
                if (!cm.requestRouteToHost(ConnectivityManager.TYPE_MOBILE_MMS, inetAddr))
                    throw new IOException("Cannot establish route to " + inetAddr + " for " + url);
            }
        }
    }

    private static int lookupHost(String hostname) {
        InetAddress inetAddress;
        try {
            inetAddress = InetAddress.getByName(hostname);
        } catch (UnknownHostException e) {
            return -1;
        }
        byte[] addrBytes;
        int addr;
        addrBytes = inetAddress.getAddress();
        addr = ((addrBytes[3] & 0xff) << 24) | ((addrBytes[2] & 0xff) << 16) | ((addrBytes[1] & 0xff) << 8) | (addrBytes[0] & 0xff);
        return addr;
    }

    private HashSet<String> getRecipients(GenericPdu pdu, TelephonyManager tMgr) {
        PduHeaders header = pdu.getPduHeaders();
        int[] ADDRESS_FIELDS = new int[] {
                PduHeaders.TO,
                PduHeaders.FROM,
                PduHeaders.CC,
                PduHeaders.BCC,
        };
        HashMap<Integer, EncodedStringValue[]> addressMap = new HashMap<>(ADDRESS_FIELDS.length);
        for (int addrType : ADDRESS_FIELDS) {
            EncodedStringValue[] array = null;
            if (addrType == PduHeaders.FROM) {
                EncodedStringValue v = header.getEncodedStringValue(addrType);
                if (v != null) {
                    array = new EncodedStringValue[1];
                    array[0] = v;
                }
            } else {
                array = header.getEncodedStringValues(addrType);
            }
            addressMap.put(addrType, array);
        }
        HashSet<String> recipients = new HashSet<>();
        loadRecipients(PduHeaders.FROM, recipients, addressMap, false, tMgr);
        loadRecipients(PduHeaders.TO, recipients, addressMap, true, tMgr);
        return recipients;
    }

    private void loadRecipients(int addressType, HashSet<String> recipients, HashMap<Integer, EncodedStringValue[]> addressMap, boolean excludeMyNumber, TelephonyManager mTelephonyManager) {
        EncodedStringValue[] array = addressMap.get(addressType);
        if (array == null) {
            return;
        }
        // If the TO recipients is only a single address, then we can skip loadRecipients when
        // we're excluding our own number because we know that address is our own.
        if (excludeMyNumber && array.length == 1) {
            return;
        }
        String myNumber = excludeMyNumber ? mTelephonyManager.getLine1Number() : null;
        for (EncodedStringValue v : array) {
            if (v != null) {
                String number = v.getString();
                if ((myNumber == null || !PhoneNumberUtils.compare(number, myNumber)) && !recipients.contains(number)) {
                    // Only add numbers which aren't my own number.
                    recipients.add(number);
                }
            }
        }
    }

    private void processPduAttachments(GenericPdu mGenericPdu) throws Exception {
        if (mGenericPdu instanceof MultimediaMessagePdu) {
            PduBody body = ((MultimediaMessagePdu) mGenericPdu).getBody();
            if (body != null) {
                int partsNum = body.getPartsNum();
                for (int i = 0; i < partsNum; i++) {
                    try {
                        PduPart part = body.getPart(i);
                        if (part == null || part.getData() == null || part.getContentType() == null || part.getName() == null)
                            continue;
                        String partType = new String(part.getContentType());
                        String partName = new String(part.getName());
                        Log.d("TSDEBUG", "Part Name: " + partName);
                        Log.d("TSDEBUG", "Part Type: " + partType);
                        if (ContentType.isTextType(partType)) {
                        } else if (ContentType.isImageType(partType)) {
                        } else if (ContentType.isVideoType(partType)) {
                        } else if (ContentType.isAudioType(partType)) {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        // Bad part shouldn't ruin the party for the other parts
                    }
                }
            }
        } else {
            Log.d("TSDEBUG", "Not a MultimediaMessagePdu PDU");
        }
    }
}
