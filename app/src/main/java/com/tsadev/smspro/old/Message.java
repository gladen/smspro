package com.tsadev.smspro.old;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.Telephony;
import android.text.format.DateUtils;

import com.tsadev.smspro.util.Utils;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Message {

    private static final String typeJpeg = "image/jpeg", typePng = "image/png";
    private long id;
    private int type;
    private boolean read, seen, mms, text_only;
    private long time;
    private CharSequence content = "", formattedTime = "";
    private Bitmap mms_picture, mms_picture_full;
    public enum Format {
        JPEG, PNG
    }

    private Format photoFormat;

    public long getId() { return id; }
    public int getType() { return type; }
    public long getTime() { return time; }
    public String getFormattedTime() { return formattedTime.toString(); }
    public String getContent() { return content.toString(); }

    public boolean isMMS() { return this.mms; }
    public boolean isRead() { return this.read; }
    public boolean isSeen() { return this.seen; }
    public void setId(long id) { this.id = id; }
    public void setType(int type) { this.type = type; }
    public void setTime(long time) {
        this.time = time;
        if (DateUtils.isToday(time)) formattedTime = new SimpleDateFormat("h:mm a", Locale.US).format(time);
        else formattedTime = new SimpleDateFormat("MMM d", Locale.US).format(time);
    }
    public void setContent(CharSequence content) { this.content = content; }
    public void setMms(boolean mms) { this.mms = mms; }
    public void setRead(boolean read) { this.read = read; }
    public void setSeen(boolean seen) { this.seen = seen; }


    public Bitmap getMMSPicture() { return this.mms_picture; }
    public Bitmap getFullSizeMMSPicture() { return this.mms_picture_full; }
    public Format getPhotoFormat() { return this.photoFormat; }
    public String getExtension() {
        switch (photoFormat) {
            case JPEG:
                return ".jpg";
            case PNG:
                return ".png";
        }
        return ".png";
    }
    public boolean isTextOnly() { return this.text_only; }
    public void setMMSPicture(Bitmap mms_picture) { this.mms_picture = mms_picture; }
    public void setTextOnly(boolean text_only) { this.text_only = text_only; }

    public void getMMS(Context context) {
        String mid = "";
        Uri partURI = Uri.parse("content://mms/part");
        Cursor cursor = context.getContentResolver().query(partURI, new String[]{
                Telephony.Mms.Part._ID,
                Telephony.Mms.Part._DATA,
                Telephony.Mms.Part.TEXT,
                Telephony.Mms.Part.CONTENT_TYPE}, Telephony.Mms.Part.MSG_ID + "=?", new String[]{String.valueOf(id)}, null);
        if (cursor == null) return;
        if (cursor.moveToFirst()) {
            int idIndx = cursor.getColumnIndex(Telephony.Mms.Part._ID),
                    dataIndx = cursor.getColumnIndex(Telephony.Mms.Part._DATA),
                    textIndx = cursor.getColumnIndex(Telephony.Mms.Part.TEXT),
                    ctntIndx = cursor.getColumnIndex(Telephony.Mms.Part.CONTENT_TYPE);
            String data, text = "", tmpTxt;
            do {
                if (cursor.getString(ctntIndx).equals("application/smil")) continue;
                tmpTxt = cursor.getString(textIndx);
                if (text.isEmpty() && tmpTxt != null && !tmpTxt.isEmpty()) text = tmpTxt;
                data = cursor.getString(dataIndx);
                if (data != null && !data.isEmpty()) mid = cursor.getString(idIndx);
            } while (cursor.moveToNext());
            cursor.close();
            setContent(text);
            if (mid.isEmpty()) return;
            Uri partIDURI = Uri.parse("content://mms/part/" + mid);
            InputStream is;
            try {
                is = context.getContentResolver().openInputStream(partIDURI);
                String type = context.getContentResolver().getType(partIDURI);
                switch (type) {
                    case typePng:
                        photoFormat = Format.PNG;
                        break;
                    case typeJpeg:
                        photoFormat = Format.JPEG;
                        break;
                }
                Bitmap tmpPhoto = BitmapFactory.decodeStream(is);
                this.mms_picture_full = tmpPhoto;
                if (tmpPhoto == null) return;
                mms_picture = Utils.resize(tmpPhoto, 250, 250);
                tmpPhoto = null;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
