package com.tsadev.smspro.old;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.tsadev.smspro.R;
import com.tsadev.smspro.old.graphics.drawable.BackgroundBitmapDrawable;
import com.tsadev.smspro.util.Constants;
import com.tsadev.smspro.util.ThreadUtils;
import com.tsadev.smspro.util.Utils;
import com.tsadev.smspro.util.Vars;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class MessageViewActivity extends AppCompatActivity {

    private final List<Message> messageList = new ArrayList<>();
    private final int MENU_CALL = 1750,
            MENU_DELETE = 4288,
            MENU_FORWARD = 3503,
            MENU_COPY_TEXT = 20093,
            MENU_COPY_SIM = 2884,
            MENU_DETAILS = 8821;
    private long contactId = 0, thread_id;
    private MessageListAdapter adapter;
    private String sender = "", strippedSender = "", displayName = "";
    private Bitmap picture;
    private ListView list;
    private EditText message;
    private final BroadcastReceiver messageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            messageList.clear();
            messageList.addAll(getSMSMessages());
            adapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null
                && getIntent().getAction() != null
                && getIntent().getAction().equals(Intent.ACTION_VIEW)) {
            sender = getIntent().getStringExtra("sender");
            if (getIntent().getLongExtra("thread_id", -1) != -1) thread_id = getIntent().getLongExtra("thread_id", -1);
            else thread_id = ThreadUtils.getOrCreateThreadId(this, sender);
        }
        else return;
        strippedSender = sender.replaceAll("(\\+1)|\\D", "");
        String formattedSender;

        if (strippedSender.length() == 10) {
            formattedSender = "("
                    .concat(strippedSender.substring(0, 3))
                    .concat(") ").concat(strippedSender.substring(3, 6))
                    .concat("-")
                    .concat(strippedSender.substring(6, 10));
        } else formattedSender = sender;
        getDisplayItems();

        messageList.addAll(getSMSMessages());
        setContentView(R.layout.activity_conversation_view);
        if (getSupportActionBar() != null) {
            if (!displayName.isEmpty()) {
                getSupportActionBar().setTitle(displayName);
                getSupportActionBar().setSubtitle(formattedSender);
            } else getSupportActionBar().setTitle(formattedSender);
        }
        Bitmap src = getDisplayPicture();
        RoundedBitmapDrawable contactPic = RoundedBitmapDrawableFactory.create(getResources(), src);
        contactPic.setCornerRadius(Math.max(src.getWidth(), src.getHeight()) / 2.0f);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(contactPic);

        message = (EditText)findViewById(R.id.send_message);
        if (getIntent().getStringExtra("sms_body") != null) {
            message.setText(getIntent().getStringExtra("sms_body"));
            message.setSelection(message.length());
        }
        list = (ListView)findViewById(R.id.convo_list);
        adapter = new MessageListAdapter(this, messageList);
        list.setAdapter(adapter);

        ImageButton attach = (ImageButton)findViewById(R.id.send_attachment);
        attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MessageViewActivity.this, "Not implemented", Toast.LENGTH_SHORT).show();
            }
        });

        list.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        Button send = (Button)findViewById(R.id.send_button);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msgText = message.getText().toString();
                if (msgText.isEmpty()) {
                    Toast.makeText(MessageViewActivity.this, "Please enter a message to send", Toast.LENGTH_SHORT).show();
                    return;
                }
                message.setText("");
                TelephonyManager manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                int phoneType = manager.getPhoneType();
                SmsManager sms = SmsManager.getDefault();
                if (phoneType == TelephonyManager.PHONE_TYPE_GSM) sms.sendTextMessage(sender, null, msgText, null, null);
                else if (phoneType == TelephonyManager.PHONE_TYPE_CDMA) {
                    sms.sendMultipartTextMessage(sender, null, sms.divideMessage(msgText), null, null);
                }
                Message message = new Message();
                message.setType(2);
                message.setContent(msgText);
                message.setTime(new Date().getTime());
                messageList.add(message);
                adapter.notifyDataSetChanged();
                if (Vars.getInstance().defaultSms) {
                    ContentValues values = new ContentValues();
                    values.put(Telephony.Sms.ADDRESS, Vars.getInstance().myPhoneNumber);
                    values.put(Telephony.Sms.BODY, msgText);
                    values.put(Telephony.Sms.THREAD_ID, thread_id);
                    values.put(Telephony.Sms.STATUS, Telephony.Sms.STATUS_NONE);
                    values.put(Telephony.Sms.DATE, message.getTime());
                    values.put(Telephony.Sms.DATE_SENT, message.getTime());
                    values.put(Telephony.Sms.TYPE, Telephony.Sms.MESSAGE_TYPE_SENT);
                    values.put(Telephony.Sms.LOCKED, 0);
                    values.put("cmasexpiretime", -1);
                    values.put("cmas", 0);
                    values.putNull("cmas_category");
                    values.putNull("cmas_response");
                    values.putNull("cmas_severity");
                    values.putNull("cmas_urgency");
                    values.putNull("cmas_certainty");
                    values.put(Telephony.Sms.REPLY_PATH_PRESENT, 0);
                    values.put("priority", 0);
                    values.put(Telephony.Sms.PROTOCOL, 0);
                    values.put(Telephony.Sms.SEEN, 1);
                    values.put(Telephony.Sms.READ, 1);
                    Uri result = getContentResolver().insert(Telephony.Sms.Sent.CONTENT_URI, values);
                    if (result == null || result.toString().isEmpty()) throw new RuntimeException("What the fuck?");
                }
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sendBroadcast(new Intent(Constants.SMS_RECEIVED));
                    }
                }, 3000);

            }
        });
        if (!Vars.getInstance().backgroundPath.isEmpty()) {
            LinearLayout root = (LinearLayout)findViewById(R.id.convo_root);
            BackgroundBitmapDrawable background = new BackgroundBitmapDrawable(getResources(), BitmapFactory.decodeFile(Vars.getInstance().backgroundPath));
            root.setBackground(background);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()== list.getId()) {
            menu.setHeaderTitle("Message");
            menu.add(Menu.NONE, MENU_DELETE, 0, "Delete");
            menu.add(Menu.NONE, MENU_FORWARD, 10, "Forward");
            menu.add(Menu.NONE, MENU_COPY_TEXT, 20, "Copy text");
            //menu.add(Menu.NONE, MENU_COPY_SIM, 30, "Copy to SIM card");
            //menu.add(Menu.NONE, MENU_DETAILS, 40, "Details");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch (item.getItemId()) {
            case MENU_DELETE:
                deleteMessage(messageList.get(info.position).getId());
                messageList.clear();
                messageList.addAll(getSMSMessages());
                adapter.notifyDataSetChanged();
                return true;
            case MENU_FORWARD: {
                String text = "Fwd: ".concat(messageList.get(info.position).getContent());
                Intent i = new Intent(this, NewMessageActivity.class);
                i.putExtra("sms_body", text);
                return true;
            }
            case MENU_COPY_TEXT: {
                String text = messageList.get(info.position).getContent();
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(text, text);
                clipboard.setPrimaryClip(clip);
                return true;
            }
            case MENU_COPY_SIM:
                return true;
            case MENU_DETAILS:
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }


    private void deleteMessage(long _ID) {
        int result = getContentResolver().delete(
                Uri.withAppendedPath(Telephony.MmsSms.CONTENT_FILTER_BYPHONE_URI, sender),
                Telephony.MmsSms._ID + "=" + _ID, null);

        Log.d("TSDEBUG", "Deleted " + result + " row(s), thread: " + _ID);
    }

    private List<Message> getSMSMessages() {
        ContentResolver cr = getContentResolver();
        Cursor cursor = cr.query(
                Uri.withAppendedPath(Telephony.MmsSms.CONTENT_FILTER_BYPHONE_URI, sender),
                new String[] {Telephony.Sms._ID,
                        Telephony.MmsSms.TYPE_DISCRIMINATOR_COLUMN,
                        Telephony.Sms.DATE,
                        Telephony.Sms.TYPE,
                        Telephony.Sms.BODY}, null, null, "date ASC");
        List<Message> messageList = new ArrayList<>();
        if (cursor == null) return messageList;
        if (cursor.moveToFirst()) {
            int tTypeIndx = cursor.getColumnIndexOrThrow(Telephony.MmsSms.TYPE_DISCRIMINATOR_COLUMN);
            int idIndx = cursor.getColumnIndexOrThrow(Telephony.MmsSms._ID),
                    dateIndx = cursor.getColumnIndexOrThrow(Telephony.Sms.DATE),
                    typeIndx = cursor.getColumnIndexOrThrow(Telephony.Sms.TYPE),
                    bodyIndx = cursor.getColumnIndexOrThrow(Telephony.Sms.BODY);
            do {
                if (cursor.getString(tTypeIndx).equals("mms")) continue;
                long id = cursor.getLong(idIndx), date = cursor.getLong(dateIndx);
                int type = cursor.getInt(typeIndx);
                String message = cursor.getString(bodyIndx);
                Message msg = new Message();
                msg.setMms(false);
                msg.setId(id);
                msg.setType(type);
                msg.setTime(date);
                msg.setContent(message);
                messageList.add(msg);
            } while (cursor.moveToNext());
        }

        cursor.close();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            messageList.addAll(getMMSMessages());
            Collections.sort(messageList, Utils.MessageComparator.getComparator(Utils.MessageComparator.TIME_SORT));
        }
        return messageList;
    }

    private List<Message> getMMSMessages() {
        ContentResolver cr = this.getContentResolver();
        Cursor cursor = cr.query(Uri.parse("content://mms/"),
                new String[]{Telephony.Mms._ID,
                        Telephony.Mms.DATE,
                        Telephony.Mms.MESSAGE_BOX,
                        Telephony.Mms.TEXT_ONLY,
                        Telephony.Mms.SEEN},
                Telephony.Mms.THREAD_ID + "=?", new String[]{String.valueOf(thread_id)}, null);
        List<Message> messageList = new ArrayList<>();
        if (cursor == null) return messageList;
        if (cursor.moveToFirst()) {
            int idIndx = cursor.getColumnIndex(Telephony.Mms._ID),
                    dateIndx = cursor.getColumnIndex(Telephony.Mms.DATE),
                    txtIndx = cursor.getColumnIndex(Telephony.Mms.TEXT_ONLY),
                    seenIndx = cursor.getColumnIndex(Telephony.Mms.SEEN),
                    boxIndx = cursor.getColumnIndex(Telephony.Mms.MESSAGE_BOX);
            do {
                Message message = new Message();
                message.setId(cursor.getInt(idIndx));
                message.setMms(true);
                message.setType(cursor.getInt(boxIndx));
                message.setTime(cursor.getLong(dateIndx) * 1000);
                message.setTextOnly(cursor.getInt(txtIndx) == 1);
                message.setSeen(cursor.getInt(seenIndx) == 1);
                messageList.add(message);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return messageList;
    }

    private void getDisplayItems() {
        if (strippedSender == null || strippedSender.isEmpty()) return;
        Uri personUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, strippedSender);
        Cursor cur = getContentResolver().query(personUri,
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID},
                null, null, null);
        if (cur == null) return;
        if (cur.moveToFirst()) {
            int nameIdx = cur.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME);
            displayName = cur.getString(nameIdx);
            contactId = cur.getLong(cur.getColumnIndex(ContactsContract.PhoneLookup._ID));
            cur.close();
            getDisplayPicture();
        }
    }

    private Bitmap getDisplayPicture() {
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
        Cursor cursor = getContentResolver().query(photoUri,
                new String[]{ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
        if (picture == null) picture = BitmapFactory.decodeResource(getResources(), R.drawable.stock_contact);
        if (cursor == null) return picture;
        try {
            if (cursor.moveToFirst()) {
                byte[] data = cursor.getBlob(0);
                if (data != null) {
                    Bitmap tmpPhoto = BitmapFactory.decodeByteArray(data, 0, data.length);
                    picture = Bitmap.createScaledBitmap(tmpPhoto, 130, 130, true);
                }
            }
        } finally {
            cursor.close();
        }
        return picture;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, MENU_CALL, Menu.NONE, "Call").setIcon(R.drawable.ic_fa_phone).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.add(Menu.NONE, 0, Menu.NONE, "Delete Messages").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (messageReceiver != null) {
            registerReceiver(messageReceiver, new IntentFilter(Constants.SMS_RECEIVED));
        }
        registerForContextMenu(list);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterForContextMenu(list);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (messageReceiver != null) {
            unregisterReceiver(messageReceiver);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
            case MENU_CALL:
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + strippedSender));
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onBackPressed() {
        if (message != null && !message.getText().toString().isEmpty()) {

            Toast.makeText(this, "Message saved as draft", Toast.LENGTH_SHORT).show();
        }
        super.onBackPressed();
    }

}
