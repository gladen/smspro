package com.tsadev.smspro.service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.tsadev.smspro.object.MmsSmsItem;
import com.tsadev.smspro.util.ContactHelper;
import com.tsadev.smspro.mms.ContentType;
import com.tsadev.smspro.object.MmsItem;
import com.tsadev.smspro.object.SmsItem;
import com.tsadev.smspro.object.ThreadItem;
import com.tsadev.smspro.util.Constants;
import com.tsadev.smspro.util.L;
import com.tsadev.smspro.util.SortedList;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MsgIntentSvc extends IntentService {

    public MsgIntentSvc() {
        this("loading_thread");
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public MsgIntentSvc(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent == null) return;
        switch (intent.getAction()) {
            case Constants.ACTION_LOAD_THREADS:
                Intent i = new Intent(Constants.ACTION_THREADS_LOADED);
                i.putParcelableArrayListExtra(Constants.THREAD_LIST_KEY, getThreadList());
                LocalBroadcastManager.getInstance(this).sendBroadcast(i);
                break;
            case Constants.ACTION_VIEW_THREAD:
                Intent t = new Intent(Constants.ACTION_MESSAGES_LOADED);
                t.putParcelableArrayListExtra(Constants.MESSAGE_LIST_KEY,
                        getMessages(intent.getStringExtra("thread_id")));
                LocalBroadcastManager.getInstance(this).sendBroadcast(t);
                break;
        }
    }

    private ArrayList<ThreadItem> getThreadList(){
        final ContentResolver cr = this.getContentResolver();
        final Cursor cursor = cr.query(Uri.parse("content://mms-sms/conversations?simple=true"),
                null, "message_count>0", null, ThreadItem.DEFAULT_SORT_ORDER);
        final SortedList<ThreadItem> mList = new SortedList<>(SortedList.SORT_ORDER.DESCENDING);
        if (cursor != null && cursor.moveToFirst()) {
            final int idIndx = cursor.getColumnIndex(ThreadItem._ID), //THIS IS THE THREAD ID
                    dateIndx = cursor.getColumnIndex(ThreadItem.DATE),
                    recpIndx = cursor.getColumnIndex(ThreadItem.RECIPIENT_IDS),
                    snipIndx = cursor.getColumnIndex(ThreadItem.SNIPPET),
                    readIndx = cursor.getColumnIndex(ThreadItem.READ),
                    archIndx = cursor.getColumnIndex(ThreadItem.ARCHIVED),
                    errIndx = cursor.getColumnIndex(ThreadItem.ERROR),
                    mmsIndx = cursor.getColumnIndex(ThreadItem.HAS_ATTACHMENT);
            final ArrayList<Long> dates = new ArrayList<>();
            do {
                final long date = cursor.getLong(dateIndx),
                        thread_id = cursor.getLong(idIndx);
                final String[] rIds = cursor.getString(recpIndx).split("\\s+");
                final String snippet = cursor.getString(snipIndx);
                boolean hasAttachments = cursor.getInt(mmsIndx) == 1;
                if (cursor.getInt(archIndx) == 1) continue;
                ThreadItem mItem = new ThreadItem();
                mItem.setThreadId(thread_id);
                mItem.setRecipientIds(cursor.getString(recpIndx));
                mItem.setSnippet(snippet);
                mItem.setRead((cursor.getInt(readIndx) == 1));
                mItem.setArchived((cursor.getInt(archIndx) == 1));
                mItem.setError(cursor.getInt(errIndx));
                mItem.setHasAttachments(hasAttachments);
                if (rIds.length > 1) getGroupMmsInfo(mItem, rIds);
                else {
                    if ((snippet == null || snippet.isEmpty()) && hasAttachments) getMmsSnippet(mItem);
                    ContactHelper.Contact contact = ContactHelper.getContactFromThreadId(this, thread_id);
                    mItem.setDisplayName(contact.getDisplayName());
                    mItem.setLookupKey(contact.getLookupKey());
                    mItem.setNormalNumber(contact.getNormalizedNumber());
                    if (contact.getLookupKey() != null && !contact.getLookupKey().isEmpty()) getDisplayPicture(mItem);
                }

                if (dates.contains(date / 10000)) {
                    mItem.setDate(getCorrectDate(thread_id));
                    mList.add(mItem);
                } else {
                    dates.add(date / 10000);
                    mItem.setDate(date);
                    mList.add(mItem);
                }

            } while (cursor.moveToNext());
            cursor.close();
        }
        return mList;
    }

    private long getCorrectDate(long thread_id) {
        long result = -1;
        final ContentResolver cr = getContentResolver();
        Cursor phoneCursor = cr.query(Telephony.Sms.CONTENT_URI,
                new String[] {
                        Telephony.Sms.DATE },
                "thread_id=?",
                new String[]{String.valueOf(thread_id)}, "date DESC LIMIT 1");
        if (phoneCursor != null && phoneCursor.moveToFirst()) {
            int dateIndx = phoneCursor.getColumnIndex(Telephony.Sms.DATE);
            result = phoneCursor.getLong(dateIndx);
            phoneCursor.close();
        }
        return result;
    }

    private void getGroupMmsInfo(ThreadItem t, String[] rIds) {
        final ContentResolver cr = getContentResolver();
        Cursor cursor;
        List<String> addresses = new ArrayList<>();
        for (String s : rIds) {
            cursor = cr.query(Constants.CANONICAL_ADDRESSES_URI,
                    new String[]{ Telephony.CanonicalAddressesColumns.ADDRESS },
                    "_id=?", new String[]{s}, "address DESC LIMIT 1");
            if (cursor != null && cursor.moveToFirst()) {
                addresses.add(cursor.getString(1));
                cursor.close();
            }
            StringBuilder sb = new StringBuilder();
            for (int i=0; i < addresses.size(); i++) {
                String tmp = addresses.get(i);
                sb.append(ContactHelper.getDisplayNameFromNumber(this, tmp));
                if (i < addresses.size() - 1) sb.append(", ");
            }
            t.setDisplayName(sb.toString());
        }
        addresses.clear();
        addresses = null;

        String mmsId = "";
        // Now to get most recent message in group (including sender's name/number)
        cursor = cr.query(Telephony.Mms.CONTENT_URI,
                new String[]{
                        MmsItem._ID,
                }, "thread_id=?", new String[]{String.valueOf(t.getThreadId())}, "_id DESC LIMIT 1");
        if (cursor != null && cursor.moveToFirst()) {
            mmsId = cursor.getString(0);
            cursor.close();
        }
        if (mmsId != null && !mmsId.isEmpty()) {
            final String FROM = "137";
            String name = "", number = "";
            cursor = cr.query(Constants.ADDR_URI(mmsId),
                    new String[]{
                            Telephony.Mms.Addr.ADDRESS
                    }, "type=?", new String[]{FROM}, "address LIMIT 1");
            if (cursor != null && cursor.moveToFirst()) {
                number = cursor.getString(0);
                name = ContactHelper.getDisplayNameFromNumber(this, number);
                cursor.close();
            }
            cursor = cr.query(Constants.PART_URI,
                    new String[]{
                            Telephony.Mms.Part.TEXT
                    },
                    "mid=? AND seq=? AND ct=?", new String[]{mmsId, "0", "text/plain"}, "text LIMIT 1");
            if (cursor != null && cursor.moveToFirst()) {
                t.setSnippet(name + ": " + cursor.getString(0));
                cursor.close();
            }
        }
    }

    private void getDisplayPicture(ThreadItem t) {
            Uri photoUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI,
                    t.getLookupKey());
            InputStream is = ContactsContract.Contacts
                    .openContactPhotoInputStream(getContentResolver(), photoUri, true);

            try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
                byte[] buffer = new byte[0xFFFF];

                for (int len; (len = is.read(buffer)) != -1;)
                    os.write(buffer, 0, len);

                os.flush();

                t.setPhotoBytes(os.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                //Do nothing
            }
    }

    private void getMmsSnippet(ThreadItem t) {
        String pid = "";
        ContentResolver cr = getContentResolver();
        Cursor cursor = cr.query(Telephony.Mms.CONTENT_URI,
                new String[]{
                        MmsItem._ID,
                }, "thread_id=?", new String[]{String.valueOf(t.getThreadId())}, "date DESC LIMIT 1");
        if (cursor != null && cursor.moveToFirst()) {
            pid = cursor.getString(cursor.getColumnIndex(MmsItem._ID));
            cursor.close();
        }
        if (pid != null && !pid.isEmpty()) {
            cursor = cr.query(Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, "part"),
                    new String[]{
                            Telephony.Mms.Part.TEXT
                    },
                    "mid=? AND seq=? AND ct=?", new String[]{pid, "0", "text/plain"}, null);
            if (cursor != null && cursor.moveToFirst()) {
                t.setSnippet("Picture: " + cursor.getString(cursor.getColumnIndex(Telephony.Mms.Part.TEXT)));
                cursor.close();
            }
        }
    }



    private ArrayList<MmsSmsItem> getMessages(String thread_id) {
        SortedList<MmsSmsItem> list = new SortedList<>(SortedList.SORT_ORDER.ASCENDING);
        list.addAll(getSMSMessages(thread_id));
        list.addAll(getMMSMessages(thread_id));
        return list;
    }

    private ArrayList<MmsSmsItem> getSMSMessages(String thread_id) {
        ContentResolver cr = getContentResolver();
        Cursor cursor = cr.query(Telephony.Sms.CONTENT_URI,
                new String[]{
                        SmsItem.ADDRESS,
                        SmsItem.BODY,
                        SmsItem.DATE,
                        SmsItem.DATE_SENT,
                        SmsItem.ERROR_CODE,
                        SmsItem.LOCKED,
                        SmsItem.READ,
                        SmsItem.SEEN,
                        SmsItem.STATUS,
                        SmsItem.SUBJECT,
                        SmsItem.TYPE
                }, "thread_id=?", new String[]{thread_id}, null);
        ArrayList<MmsSmsItem> smsItems = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                MmsSmsItem item = new MmsSmsItem();
                item.addr = cursor.getString(0);
                item.mText = cursor.getString(1);
                item.setDate(cursor.getLong(2));
                item.mDateSent = cursor.getLong(3);
                item.errCode = cursor.getInt(4);
                item.mLocked = cursor.getInt(5) == 1;
                item.mRead = cursor.getInt(6) == 1;
                item.mSeen = cursor.getInt(7) == 1;
                item.mStat = cursor.getInt(8);
                item.mSub = cursor.getString(9);
                item.type = cursor.getInt(10);
                item.mms = false;
                smsItems.add(item);
            } while (cursor.moveToNext());
            cursor.close();
        }
        return smsItems;
    }

    private ArrayList<MmsSmsItem> getMMSMessages(String thread_id) {
        ContentResolver cr = getContentResolver();
        Cursor cursor = cr.query(Telephony.Mms.CONTENT_URI,
                new String[]{
                        MmsItem._ID, //These are columns in the content://mms Content URI, they are different in
                        MmsItem.DATE, //the content://mms/part Content URI
                        MmsItem.DATE_SENT,
                        MmsItem.MESSAGE_BOX,
                        MmsItem.MESSAGE_ID,
                        MmsItem.SUBJECT,
                        MmsItem.SUBJECT_CHARSET,
                        MmsItem.CONTENT_TYPE,
                        MmsItem.EXPIRY,
                        MmsItem.MESSAGE_CLASS,
                        MmsItem.MESSAGE_TYPE,
                        MmsItem.MMS_VERSION,
                        MmsItem.MESSAGE_SIZE,
                        MmsItem.PRIORITY,
                        MmsItem.READ_REPORT,
                        MmsItem.REPORT_ALLOWED,
                        MmsItem.RESPONSE_STATUS,
                        MmsItem.STATUS,
                        MmsItem.TRANSACTION_ID,
                        MmsItem.RETRIEVE_STATUS,
                        MmsItem.RETRIEVE_TEXT,
                        MmsItem.RETRIEVE_TEXT_CHARSET,
                        MmsItem.READ_STATUS,
                        MmsItem.CONTENT_CLASS,
                        MmsItem.RESPONSE_TEXT,
                        MmsItem.DELIVERY_TIME,
                        MmsItem.DELIVERY_REPORT,
                        MmsItem.SEEN,
                        MmsItem.TEXT_ONLY,
                        MmsItem.SUBSCRIPTION_ID,
                        MmsItem.READ,
                        MmsItem.LOCKED
                }, "thread_id=?", new String[]{thread_id}, "date ASC");
        ArrayList<MmsSmsItem> items = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            int idIndx = cursor.getColumnIndex(MmsItem._ID),
                    dateIndx = cursor.getColumnIndex(MmsItem.DATE),
                    datSntIndx = cursor.getColumnIndex(MmsItem.DATE_SENT),
                    boxIndx = cursor.getColumnIndex(MmsItem.MESSAGE_BOX),
                    mIdIndx = cursor.getColumnIndex(MmsItem.MESSAGE_ID),
                    subIndx = cursor.getColumnIndex(MmsItem.SUBJECT),
                    subCsIndx = cursor.getColumnIndex(MmsItem.SUBJECT_CHARSET),
                    ctIndx = cursor.getColumnIndex(MmsItem.CONTENT_TYPE),
                    expIndx = cursor.getColumnIndex(MmsItem.EXPIRY),
                    mClsIndx = cursor.getColumnIndex(MmsItem.MESSAGE_CLASS),
                    mTypeIndx = cursor.getColumnIndex(MmsItem.MESSAGE_TYPE),
                    mVerIndx = cursor.getColumnIndex(MmsItem.MMS_VERSION),
                    mSizeIndx = cursor.getColumnIndex(MmsItem.MESSAGE_SIZE),
                    priIndx = cursor.getColumnIndex(MmsItem.PRIORITY),
                    rrIndx = cursor.getColumnIndex(MmsItem.READ_REPORT),
                    rptaIndx = cursor.getColumnIndex(MmsItem.REPORT_ALLOWED),
                    rspStIndx = cursor.getColumnIndex(MmsItem.RESPONSE_STATUS),
                    stIndx = cursor.getColumnIndex(MmsItem.STATUS),
                    trIdIndx = cursor.getColumnIndex(MmsItem.TRANSACTION_ID),
                    rtStIndx = cursor.getColumnIndex(MmsItem.RETRIEVE_STATUS),
                    rtTxtIndx = cursor.getColumnIndex(MmsItem.RETRIEVE_TEXT),
                    rtTxtCsIndx = cursor.getColumnIndex(MmsItem.RETRIEVE_TEXT_CHARSET),
                    rdStIndx = cursor.getColumnIndex(MmsItem.READ_STATUS),
                    ctClsIndx = cursor.getColumnIndex(MmsItem.CONTENT_CLASS),
                    rspTxtIndx = cursor.getColumnIndex(MmsItem.RESPONSE_TEXT),
                    dTmIndx = cursor.getColumnIndex(MmsItem.DELIVERY_TIME),
                    dRptIndx = cursor.getColumnIndex(MmsItem.DELIVERY_REPORT),
                    seenIndx = cursor.getColumnIndex(MmsItem.SEEN),
                    txtOnlIndx = cursor.getColumnIndex(MmsItem.TEXT_ONLY),
                    subIdIndx = cursor.getColumnIndex(MmsItem.SUBSCRIPTION_ID),
                    readIndx = cursor.getColumnIndex(MmsItem.READ),
                    lckdIndx = cursor.getColumnIndex(MmsItem.LOCKED);
            do {
                MmsSmsItem mItem = new MmsSmsItem();
                mItem.id = cursor.getLong(idIndx);
                mItem.setDate(cursor.getLong(dateIndx) * 1000);
                mItem.mDateSent = cursor.getLong(datSntIndx);
                mItem.mBox = cursor.getInt(boxIndx);
                mItem.mId = cursor.getString(mIdIndx);
                mItem.mSub = cursor.getString(subIndx);
                mItem.mSubCs = cursor.getInt(subCsIndx);
                mItem.mCtType = cursor.getString(ctIndx);
                mItem.mExp = cursor.getLong(expIndx);
                mItem.mCls = cursor.getString(mClsIndx);
                mItem.mType = cursor.getInt(mTypeIndx);
                mItem.mVer = cursor.getInt(mVerIndx);
                mItem.mSize = cursor.getInt(mSizeIndx);
                mItem.mPri = cursor.getInt(priIndx);
                mItem.mReadRpt = cursor.getInt(rrIndx) == 1;
                mItem.mRptAllowed = cursor.getInt(rptaIndx) == 1;
                mItem.mRespStat = cursor.getInt(rspStIndx);
                mItem.mStat = cursor.getInt(stIndx);
                mItem.mTranId = cursor.getString(trIdIndx);
                mItem.mRetStat = cursor.getInt(rtStIndx);
                mItem.mRetTxt = cursor.getString(rtTxtIndx);
                mItem.mRetTxtCs = cursor.getInt(rtTxtCsIndx);
                mItem.mReadStat = cursor.getInt(rdStIndx);
                mItem.mCtCls = cursor.getInt(ctClsIndx);
                mItem.mRespTxt = cursor.getString(rspTxtIndx);
                mItem.mDelTime = cursor.getInt(dTmIndx);
                mItem.mDelRpt = cursor.getInt(dRptIndx);
                mItem.mSeen = cursor.getInt(seenIndx) == 1;
                mItem.mTxtOnl = cursor.getInt(txtOnlIndx) == 1;
                mItem.mSubId = cursor.getLong(subIdIndx);
                mItem.mRead = cursor.getInt(readIndx) == 1;
                mItem.mLocked = cursor.getInt(lckdIndx) == 1;
                mItem.mms = true;
                items.add(mItem);
            } while (cursor.moveToNext());
            cursor.close();

            for (MmsSmsItem m : items) {
                getMmsData(m);
            }
        }
        return items;
    }

    private void getMmsData(MmsSmsItem mItem) {
        ContentResolver cr = getContentResolver();
        Cursor cursor = cr.query(Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, "part"),
                new String[]{
                        Telephony.Mms.Part._ID,
                        Telephony.Mms.Part.CONTENT_TYPE,
                        Telephony.Mms.Part.CHARSET,
                        Telephony.Mms.Part._DATA,
                        Telephony.Mms.Part.TEXT
                },
                "mid=? AND seq=?", new String[]{String.valueOf(mItem.id), "0"}, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                String contentType = cursor.getString(cursor.getColumnIndex(Telephony.Mms.Part.CONTENT_TYPE));
                switch (contentType) {
                    case ContentType.TEXT_HTML:
                    case ContentType.TEXT_PLAIN:
                        if (mItem.mText == null || mItem.mText.isEmpty() || mItem.mText.equals(" ")) {
                            String msgText = cursor.getString(cursor.getColumnIndex(Telephony.Mms.Part.TEXT));
                            if (msgText != null && !msgText.isEmpty() && msgText.length() > 0) {
                                mItem.mText = msgText;
                            }
                        }
                        continue;
                    case ContentType.TEXT_VCALENDAR:
                    case ContentType.TEXT_VCARD:
                        continue;
                    case ContentType.VIDEO_3G2:
                    case ContentType.VIDEO_3GPP:
                    case ContentType.VIDEO_H263:
                    case ContentType.VIDEO_MP4:
                    case ContentType.VIDEO_UNSPECIFIED:
                        continue;
                    case ContentType.IMAGE_X_MS_BMP:
                    case ContentType.IMAGE_WBMP:
                    case ContentType.IMAGE_GIF:
                    case ContentType.IMAGE_JPEG:
                    case ContentType.IMAGE_JPG:
                    case ContentType.IMAGE_PNG:
                    case ContentType.IMAGE_UNSPECIFIED:
                        String partId = cursor.getString(cursor.getColumnIndex(Telephony.Mms.Part._ID));
                        if (partId != null && !partId.isEmpty()) mItem.mmsPartIds.add(partId);

                }
            } while (cursor.moveToNext());
            cursor.close();
        }
    }
}
