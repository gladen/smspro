package com.tsadev.smspro.service;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.tsadev.smspro.old.NotificationService;
import com.tsadev.smspro.util.Constants;
import com.tsadev.smspro.util.ThreadUtils;
import com.tsadev.smspro.util.Vars;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MessagingService {


    public class ReceiverSms extends BroadcastReceiver {

        private final ContactInfo info = new ContactInfo();

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Telephony.Sms.Intents.SMS_DELIVER_ACTION)) {
                final Bundle bundle = intent.getExtras();
                String address = "", message = "";
                try {
                    if (bundle != null) {
                        final byte[][] pdusObj = (byte[][]) bundle.get("pdus");
                        assert pdusObj != null;

                        for (byte[] messagePdu : pdusObj) {
                            Intent messageIntent = new Intent();
                            SmsMessage currentMessage = SmsMessage
                                    .createFromPdu(messagePdu, intent.getStringExtra("format"));
                            address = currentMessage.getDisplayOriginatingAddress();
                            message = currentMessage.getDisplayMessageBody();
                            long thread_id = ThreadUtils.getOrCreateThreadId(context, address);
                            getDisplayNameAndPicture(context, address);
                            String sender = "";
                            if (info.displayName.isEmpty()) {
                                String tmpNum = currentMessage.getDisplayOriginatingAddress();
                                sender = PhoneNumberUtils.formatNumber(tmpNum, Locale.getDefault().getISO3Country());
                                messageIntent.putExtra("displayName", sender);
                            } else messageIntent.putExtra("displayName", info.displayName);

                            messageIntent.putExtra("sender", address);
                            messageIntent.putExtra("message", message);
                            messageIntent.putExtra("thread_id", thread_id);

                            if (Vars.getInstance().defaultSms) {
                                SmsManager.getDefault()
                                        .injectSmsPdu(messagePdu, intent.getStringExtra("format"), null);
                            }

                            ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                            List<ActivityManager.RunningTaskInfo> tasks = manager.getRunningTasks(1);
                            String topActivityName = tasks.get(0).topActivity.getPackageName();
                            if (!(topActivityName.equalsIgnoreCase("com.tsadev.smspro"))) {
                                if (Vars.getInstance().notifications)
                                    NotificationService.sendNotification(context, messageIntent, info.picture);
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e("TSDEBUG", "SMS Exception: " + e);
                }
                context.sendBroadcast(new Intent(Constants.SMS_RECEIVED));
            } else if (intent.getAction().equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION))
                context.sendBroadcast(new Intent(Constants.SMS_RECEIVED));
            else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
                Log.d("TSDEBUG", "Boot completed intent received");
            }
        }


        public void getDisplayNameAndPicture(Context context, String phoneNumber) {
            Uri personUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                    phoneNumber.replace("(", "")
                            .replace(")", "")
                            .replace("-", "")
                            .replace(" ", "")
                            .replace("+1", ""));
            Cursor cur = context.getContentResolver().query(personUri,
                    new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID}, null, null, null);
            if (cur == null) return;
            if (cur.moveToFirst()) {
                info.displayName = cur.getString(cur.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                info.id = cur.getLong(cur.getColumnIndex(ContactsContract.PhoneLookup._ID));
                cur.close();
            }
            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, info.id);
            Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
            Cursor cursor = context.getContentResolver().query(photoUri,
                    new String[]{ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
            if (cursor == null) return;
            try {
                if (cursor.moveToFirst()) {
                    byte[] data = cursor.getBlob(0);
                    if (data != null) {
                        Bitmap tmpPic = BitmapFactory.decodeByteArray(data, 0, data.length);
                        info.picture = Bitmap.createScaledBitmap(tmpPic, 130, 130, true);
                    }
                }
            } finally {
                cursor.close();
            }
        }

        private class ContactInfo {
            public String displayName = "";
            public Bitmap picture;
            public long id;
        }

    }

}
