package com.tsadev.smspro;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.tsadev.smspro.util.L;

import java.lang.reflect.Field;

public class ContentProviderHelper {

    private Context context;

    private static ContentProviderHelper helper;

    private ContentProviderHelper () {
    }

    private ContentProviderHelper(Context context) {
        this.context = context;
    }

    public static ContentProviderHelper getInstance(Context context) {
        if (helper == null) {
            helper = new ContentProviderHelper(context);
        } else helper.setContext(context);
        return helper;
    }


    private void setContext(Context con) {
        context = con;
    }


    public void runQuery() {
        try {
            Class c = Class.forName("MmsSmsProvider");
            Field f = c.getDeclaredField("db");
            f.setAccessible(true);
            SQLiteDatabase db = (SQLiteDatabase) f.get(c);
            L.d("WIN! Path: " + db.getPath());
        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
