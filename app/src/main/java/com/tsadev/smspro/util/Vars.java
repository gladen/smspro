package com.tsadev.smspro.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.tsadev.smspro.object.MessageItem;

import java.util.ArrayList;

public class Vars {

    private static final Vars instance = new Vars();
    public boolean defaultSms = false,
            readSMS = false,
            sendSMS = false,
            readContacts = false,
            writeContacts = false,
            call = false,
            notifications;
    public String backgroundPath = "", myPhoneNumber = "", ringtonePath = "", vibrateMode = "", mmsc = "";
    public int subscriptionId = -1;

    public static Vars getInstance(){ return instance; }

    private Vars () {}

    public void getPreferences(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        notifications = prefs.getBoolean("notifications", false);
        backgroundPath = prefs.getString("backgroundPath", "");
        ringtonePath = prefs.getString("ringtonePath", "");
        vibrateMode = prefs.getString("vibrateMode", "");
    }
}
