package com.tsadev.smspro.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.telephony.PhoneNumberUtils;

public class ContactHelper {

    //Never instantiated
    private ContactHelper() {
    }

    public static Contact getContactFromThreadId(Context context, long thread_id) {
        String name = "", number = "", normalNumber = "", lookupKey = "";
        ContentResolver cr = context.getContentResolver();
        Cursor phoneCursor = cr.query(Telephony.Sms.CONTENT_URI,
                new String[] {
                        Telephony.Sms.ADDRESS },
                "thread_id=?",
                new String[]{String.valueOf(thread_id)}, null);
        if (phoneCursor != null && phoneCursor.moveToFirst()) {
            int numIndx = phoneCursor.getColumnIndex(Telephony.Sms.ADDRESS);
            number = phoneCursor.getString(numIndx);
            phoneCursor.close();
        }

        if (number.isEmpty()) {
            String mmsId = "";
            int box = -1;
            phoneCursor = cr.query(Telephony.Mms.CONTENT_URI,
                    new String[]{
                            Telephony.Mms._ID,
                            Telephony.Mms.MESSAGE_BOX
                    }, "thread_id=?", new String[]{String.valueOf(thread_id)}, null);
            if (phoneCursor != null && phoneCursor.moveToFirst()) {
                mmsId = phoneCursor.getString(phoneCursor.getColumnIndex(Telephony.Mms._ID));
                box = phoneCursor.getInt(phoneCursor.getColumnIndex(Telephony.Mms.MESSAGE_BOX));
                phoneCursor.close();
            }
            if (mmsId != null && !mmsId.isEmpty()) {
                final String FROM = "137";
                final String TO = "151";
                String who = "";

                switch (box) {
                    case Telephony.Mms.MESSAGE_BOX_INBOX:
                        who = FROM;
                        break;
                    case Telephony.Mms.MESSAGE_BOX_DRAFTS:
                    case Telephony.Mms.MESSAGE_BOX_FAILED:
                    case Telephony.Mms.MESSAGE_BOX_OUTBOX:
                    case Telephony.Mms.MESSAGE_BOX_SENT:
                        who = TO;
                        break;
                }
                phoneCursor = cr.query(Constants.ADDR_URI(mmsId),
                        new String[]{
                                Telephony.Mms.Addr.ADDRESS
                        }, "type=?", new String[]{who}, null);
                if (phoneCursor != null && phoneCursor.moveToFirst()) {
                    number = phoneCursor.getString(phoneCursor.getColumnIndex(Telephony.Mms.Addr.ADDRESS));
                    name = number;
                    phoneCursor.close();
                }
            }
        } else {
            phoneCursor = cr.query(
                    Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number)),
                    new String[]{
                            ContactsContract.PhoneLookup.LOOKUP_KEY,
                            ContactsContract.PhoneLookup.DISPLAY_NAME,
                            ContactsContract.PhoneLookup.NORMALIZED_NUMBER },
                    null, null, null);

            if (phoneCursor != null && phoneCursor.moveToFirst()) {
                int lookupIndx = phoneCursor.getColumnIndex(ContactsContract.PhoneLookup.LOOKUP_KEY),
                        nameIndx = phoneCursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME),
                        numIndx = phoneCursor.getColumnIndex(ContactsContract.PhoneLookup.NORMALIZED_NUMBER);
                lookupKey = phoneCursor.getString(lookupIndx);
                name = phoneCursor.getString(nameIndx);
                normalNumber = phoneCursor.getString(numIndx);
                phoneCursor.close();
            } else {
                name = PhoneNumberUtils.formatNumber(number, "US");
                if (name == null || name.isEmpty()) {
                    name = number;
                }
            }
        }
        return new Contact(name, number, normalNumber, lookupKey);
    }

    public static String getDisplayNameFromNumber(Context context, String number) {
        String name = "";
        ContentResolver cr = context.getContentResolver();
        Cursor phoneCursor = cr.query(
                Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number)),
                new String[]{
                        ContactsContract.PhoneLookup.DISPLAY_NAME },
                null, null, null);

        if (phoneCursor != null && phoneCursor.moveToFirst()) {
            int nameIndx = phoneCursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME);
            name = phoneCursor.getString(nameIndx);
            phoneCursor.close();
        }
        if (name != null && !name.isEmpty()) return name;
        else return number;
    }



    public static class Contact {

        private static String displayName = "", number = "", normalizedNumber = "", lookupKey = "";

        private Contact(String name, String num, String normNum, String key) {
            displayName = name;
            number = num;
            normalizedNumber = normNum;
            lookupKey = key;
        }

        public String getDisplayName() {
            return displayName;
        }

        public String getNumber() {
            return number;
        }

        public String getNormalizedNumber() {
            return normalizedNumber;
        }

        public String getLookupKey() {
            return lookupKey;
        }
    }
}
