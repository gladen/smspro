package com.tsadev.smspro.util;

import java.util.ArrayList;
import java.util.Collection;

public class SortedList<E extends Comparable<E>> extends ArrayList<E> {

    public enum SORT_ORDER { ASCENDING, DESCENDING }

    private SORT_ORDER order;

    private SortedList() {

    }

    public SortedList(SORT_ORDER order) {
        this.order = order;
    }

    @Override
    public boolean add(E object) {
        int position;
        if (size() == 0) return super.add(object);
        for (E obj : this) {
            switch (order) {
                case DESCENDING:
                if (object.compareTo(obj) > 0) {
                    position = this.indexOf(obj);
                    add(position, object);
                    return true;
                }
                break;
                case ASCENDING:
                    if (object.compareTo(obj) < 0) {
                        position = this.indexOf(obj);
                        add(position, object);
                        return true;
                    }
                    break;
            }
        }
        return super.add(object);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        for (E obj : c) {
            add(obj);
        }
        return true;
    }
}
