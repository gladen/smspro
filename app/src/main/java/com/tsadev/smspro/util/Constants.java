package com.tsadev.smspro.util;

import android.net.Uri;
import android.provider.Telephony;

public final class Constants {

    public static final String PACKAGE_NAME = "com.tsadev.smspro";

    public static final String ACTION_LOAD_THREADS = PACKAGE_NAME + ".LOAD_THREADS";

    public static final String ACTION_VIEW_THREAD = PACKAGE_NAME + ".VIEW_THREAD";

    public static final String ACTION_THREADS_LOADED = PACKAGE_NAME + ".THREADS_LOADED";

    public static final String ACTION_MESSAGES_LOADED = PACKAGE_NAME + ".MESSAGES_LOADED";

    public static final String SMS_RECEIVED = PACKAGE_NAME + ".SMS_RECEIVED",
            MMS_RECEIVED = PACKAGE_NAME + ".MMS_RECEIVED";

    public static final String THREAD_LIST_KEY = PACKAGE_NAME + ".thread_list";

    public static final String MESSAGE_LIST_KEY = PACKAGE_NAME + ".message_list";

    public static final Uri PART_URI = Uri.withAppendedPath(Telephony.Mms.CONTENT_URI, "part");

    public static final Uri CANONICAL_ADDRESSES_URI = Uri.withAppendedPath(Telephony.MmsSms.CONTENT_URI, "canonical-addresses");

    public static final Uri ADDR_URI(String mmsId) {
        return Telephony.Mms.CONTENT_URI.buildUpon().appendPath(mmsId).appendPath("addr").build();
    }

}
