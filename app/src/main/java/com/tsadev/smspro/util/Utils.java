package com.tsadev.smspro.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.Telephony;
import android.support.design.widget.Snackbar;
import android.telephony.SubscriptionInfo;
import android.util.DisplayMetrics;
import android.view.View;

import com.tsadev.smspro.object.MessageItem;
import com.tsadev.smspro.object.MmsSmsItem;
import com.tsadev.smspro.old.Message;
import com.tsadev.smspro.old.SubListAdapter;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Utils {

    public static final int REQUEST_DEFAULT = 4500;

    public static void showSubSelector(Context context, List<SubscriptionInfo> subInfo) {

        AlertDialog.Builder b = new AlertDialog.Builder(context);
        b.setTitle("Select SIM card to use");
        b.setAdapter(new SubListAdapter(context, subInfo), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int subscriptionId) {
                Vars.getInstance().subscriptionId = subscriptionId;
                dialog.dismiss();
            }

        });
        b.show();
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
    /**
     * Checks if we are the default SMS app
     * @return true if we are, false if not
     */
    public static boolean checkIfDefaultSMS(Context context) {
        return Telephony.Sms.getDefaultSmsPackage(context).equals(context.getPackageName());
        // Set up a button that allows the user to change the default SMS app
    }

    public static void setDefaultSMS(Activity activity) {
        // Show the "not currently set as the default SMS app" interface
        Intent intent = new Intent(Telephony.Sms.Intents.ACTION_CHANGE_DEFAULT);
        intent.putExtra(Telephony.Sms.Intents.EXTRA_PACKAGE_NAME, activity.getPackageName());
        activity.startActivityForResult(intent, REQUEST_DEFAULT);
    }

    public static int generateViewId() {
        final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
        for (;;) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }


    public static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    {
        // Snackbar usage
        final Snackbar msg = Snackbar.make(null, "", Snackbar.LENGTH_SHORT);
        msg.setAction("DISMISS", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // There is a bug with dismiss() that causes the FAB to get stuck, so we just use setDuration()
                msg.setDuration(0);
            }
        });
        msg.show();
    }



    public enum MessageItemComparator implements Comparator<MmsSmsItem> {
        ID_SORT {
            public int compare(MmsSmsItem o1, MmsSmsItem o2) {
                return Long.valueOf(o1.id).compareTo(o2.id);
            }},
        TIME_SORT {
            public int compare(MmsSmsItem o1, MmsSmsItem o2) {
                return Long.valueOf(o1.mDate).compareTo(o2.mDate);
            }};

        public static Comparator<MmsSmsItem> decending(final Comparator<MmsSmsItem> other) {
            return new Comparator<MmsSmsItem>() {
                public int compare(MmsSmsItem o1, MmsSmsItem o2) {
                    return -1 * other.compare(o1, o2);
                }
            };
        }

        public static Comparator<MmsSmsItem> ascending(final Comparator<MmsSmsItem> other) {
            return new Comparator<MmsSmsItem>() {
                public int compare(MmsSmsItem o1, MmsSmsItem o2) {
                    return other.compare(o1, o2);
                }
            };
        }

        public static Comparator<MmsSmsItem> getComparator(final MessageItemComparator... multipleOptions) {
            return new Comparator<MmsSmsItem>() {
                public int compare(MmsSmsItem o1, MmsSmsItem o2) {
                    for (MessageItemComparator option : multipleOptions) {
                        int result = option.compare(o1, o2);
                        if (result != 0) {
                            return result;
                        }
                    }
                    return 0;
                }
            };
        }
    }

    public enum MessageComparator implements Comparator<Message> {
        ID_SORT {
            public int compare(Message o1, Message o2) {
                return Long.valueOf(o1.getId()).compareTo(o2.getId());
            }},
        TIME_SORT {
            public int compare(Message o1, Message o2) {
                return Long.valueOf(o1.getTime()).compareTo(o2.getTime());
            }};

        public static Comparator<Message> decending(final Comparator<Message> other) {
            return new Comparator<Message>() {
                public int compare(Message o1, Message o2) {
                    return -1 * other.compare(o1, o2);
                }
            };
        }

        public static Comparator<Message> ascending(final Comparator<Message> other) {
            return new Comparator<Message>() {
                public int compare(Message o1, Message o2) {
                    return other.compare(o1, o2);
                }
            };
        }

        public static Comparator<Message> getComparator(final MessageComparator... multipleOptions) {
            return new Comparator<Message>() {
                public int compare(Message o1, Message o2) {
                    for (MessageComparator option : multipleOptions) {
                        int result = option.compare(o1, o2);
                        if (result != 0) {
                            return result;
                        }
                    }
                    return 0;
                }
            };
        }
    }
}
