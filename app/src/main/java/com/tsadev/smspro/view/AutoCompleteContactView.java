package com.tsadev.smspro.view;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.SimpleAdapter;

import com.tsadev.smspro.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AutoCompleteContactView extends AutoCompleteTextView implements AdapterView.OnItemClickListener {

    private ArrayList<Map<String, String>> mPeopleList;
    private String contactName, contactNumber;
    private int charsDeleted = 0;
    private boolean resetChars = false;
    private static final String[] PROJECTION = new String[] {
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.Contacts.HAS_PHONE_NUMBER
    };



    public AutoCompleteContactView(Context context) {
        this(context, null);
    }

    public AutoCompleteContactView(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.autoCompleteTextViewStyle);
    }

    public AutoCompleteContactView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public AutoCompleteContactView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if (isInEditMode()) return;
        mPeopleList = new ArrayList<>();
        getPeopleList();
        SimpleAdapter mAdapter = new SimpleAdapter(context, mPeopleList, R.layout.contact_list,
                new String[] { "Name", "Phone" }, new int[] {
                R.id.ccontName, R.id.ccontNo});
        setAdapter(mAdapter);
        setOnItemClickListener(this);
    }

    private void getPeopleList() {
        mPeopleList.clear();
        ContentResolver cr = getContext().getContentResolver();
        Cursor cursor = cr.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                PROJECTION,
                ContactsContract.CommonDataKinds.Phone.TYPE + " = 2",
                null, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    final int nameIndx = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                    final int numIndx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    String displayName, address;
                    do {
                        displayName = cursor.getString(nameIndx);
                        address = cursor.getString(numIndx);
                        Map<String, String> NamePhoneType = new HashMap<>();
                        NamePhoneType.put("Name", displayName);
                        NamePhoneType.put("Phone", address);
                        mPeopleList.add(NamePhoneType);
                    }
                    while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }
    }

    public String getContactNumber() {
        if (contactNumber == null || contactNumber.isEmpty()) return getText().toString();
        else return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
        Uri personUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, this.contactNumber);
        Cursor cursor = getContext().getContentResolver().query(personUri,
                new String[]{ContactsContract.Contacts.DISPLAY_NAME}, null, null, null);
        if (cursor == null) return;
        if (cursor.moveToFirst()) {
            final int ctNameIndx = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
            this.contactName = cursor.getString(ctNameIndx);
        }
        cursor.close();
        String out = "" + this.contactName + " <" + this.contactNumber + ">";
        setText(out);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //noinspection unchecked
        Map<String, String> map = (Map<String, String>) parent.getItemAtPosition(position);
        String name = map.get("Name");
        String number = map.get("Phone");
        contactName = name;
        contactNumber = number;
        String out = "" + name + " <" + number + ">";
        setText(out);
        setSelection(out.length());
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        if (lengthBefore > 0 && lengthAfter == 0) {
            if (lengthAfter < lengthBefore && !resetChars) {
                charsDeleted++;
            }
        }
        else if (lengthBefore < lengthAfter) {
            charsDeleted = 0;
        }
        if (charsDeleted > 2) {
            contactNumber = "";
            charsDeleted = 0;
            resetChars = true;
            setText("");
        }
        if (resetChars) resetChars = false;
    }
}
