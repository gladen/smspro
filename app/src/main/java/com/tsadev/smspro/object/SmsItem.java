package com.tsadev.smspro.object;

import android.provider.Telephony;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SmsItem extends MessageItem implements Telephony.TextBasedSmsColumns {

    private String simpleTime;

    /*
     * Inherited variables & methods
     */

    private long id, mDate, mDateSent, mSubId;

    private boolean mRead, mLocked;

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setDate(long mDate) {
        this.mDate = mDate;
        Date timestamp = new Date(mDate);
        if (System.currentTimeMillis() - mDate < 86400000) {
            DateFormat formatter = new SimpleDateFormat("h:mm a", Locale.US);
            this.simpleTime = formatter.format(timestamp);
        } else {
            DateFormat formatter = new SimpleDateFormat("MMM d", Locale.US);
            this.simpleTime = formatter.format(timestamp);
        }
    }

    @Override
    public long getDate() {
        return mDate;
    }

    @Override
    public void setDateSent(long mDateSent) {
        this.mDateSent = mDateSent;
    }

    @Override
    public long getDateSent() {
        return mDateSent;
    }

    @Override
    public void setSubId(long mSubId) {
        this.mSubId = mSubId;
    }

    @Override
    public long getSubId() {
        return mSubId;
    }

    @Override
    public void setRead(boolean mRead) {
        this.mRead = mRead;
    }

    @Override
    public boolean isRead() {
        return mRead;
    }

    @Override
    public void setLockStatus(boolean mLocked) {
        this.mLocked = mLocked;
    }

    @Override
    public boolean isLocked() {
        return mLocked;
    }

    @Override
    public String getSimpleTime() {
        return this.simpleTime;
    }


    /*
     * Variables & methods specific to SMS messages
     */

    private int errCode, threadId, person, proto, status, type;

    private boolean replyPathPresent, seen;

    private String addr, body, creator, svcCntr, sub;

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public int getThreadId() {
        return threadId;
    }

    public void setThreadId(int threadId) {
        this.threadId = threadId;
    }

    public int getPerson() {
        return person;
    }

    public void setPerson(int person) {
        this.person = person;
    }

    public int getProto() {
        return proto;
    }

    public void setProto(int proto) {
        this.proto = proto;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isReplyPathPresent() {
        return replyPathPresent;
    }

    public void setReplyPathPresent(boolean replyPathPresent) {
        this.replyPathPresent = replyPathPresent;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getText() {
        return body;
    }

    public void setText(String body) {
        this.body = body;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getSvcCntr() {
        return svcCntr;
    }

    public void setSvcCntr(String svcCntr) {
        this.svcCntr = svcCntr;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }


}
