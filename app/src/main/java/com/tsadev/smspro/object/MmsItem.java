package com.tsadev.smspro.object;

import android.provider.Telephony;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MmsItem extends MessageItem implements Telephony.BaseMmsColumns {

    private String simpleTime;

    /*
     * Inherited variables & methods
     */

    private long id, mDate, mDateSent, mSubId;

    private boolean mRead, mLocked;

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setDate(long mDate) {
        this.mDate = mDate;
        Date timestamp = new Date(mDate);
        if (System.currentTimeMillis() - mDate < 86400000) {
            DateFormat formatter = new SimpleDateFormat("h:mm a", Locale.US);
            this.simpleTime = formatter.format(timestamp);
        } else {
            DateFormat formatter = new SimpleDateFormat("MMM d", Locale.US);
            this.simpleTime = formatter.format(timestamp);
        }
    }

    @Override
    public long getDate() {
        return mDate;
    }

    @Override
    public void setDateSent(long mDateSent) {
        this.mDateSent = mDateSent * 1000;
    }

    @Override
    public long getDateSent() {
        return mDateSent;
    }

    @Override
    public void setSubId(long mSubId) {
        this.mSubId = mSubId;
    }

    @Override
    public long getSubId() {
        return mSubId;
    }

    @Override
    public void setRead(boolean mRead) {
        this.mRead = mRead;
    }

    @Override
    public boolean isRead() {
        return mRead;
    }

    @Override
    public void setLockStatus(boolean mLocked) {
        this.mLocked = mLocked;
    }

    @Override
    public boolean isLocked() {
        return mLocked;
    }

    @Override
    public String getSimpleTime() {
        return this.simpleTime;
    }


    /*
     * Variables & methods specific to MMS messages
     */

    private int mBox, mSubCs, mType, mVer,
            mSize, mPri, mRespStat, mStat,
            mRetStat, mRetTxtCs, mReadStat,
            mCtCls, mDelTime, mDelRpt;

    private long mExp;

    private boolean mReadRpt, mRptAllowed,
            mSeen, mTxtOnl;

    private String mId, mSub, mCtType, mCls,
            mTranId, mRetTxt, mRespTxt;

    public void setBox(int mBox) {
        this.mBox = mBox;
    }

    /**
     * Returns the box that the given message is stored in
     * @return the box of given message, one of {@link android.provider.Telephony.Mms#MESSAGE_BOX_ALL},
     *  {@link android.provider.Telephony.Mms#MESSAGE_BOX_DRAFTS},
     *  {@link android.provider.Telephony.Mms#MESSAGE_BOX_FAILED},
     *  {@link android.provider.Telephony.Mms#MESSAGE_BOX_INBOX},
     *  {@link android.provider.Telephony.Mms#MESSAGE_BOX_OUTBOX}, or
     *  {@link android.provider.Telephony.Mms#MESSAGE_BOX_SENT}
     */
    public int getBox() {
        return this.mBox;
    }

    public int getSubCs() {
        return mSubCs;
    }

    public void setSubCs(int mSubCs) {
        this.mSubCs = mSubCs;
    }

    public int getType() {
        return mType;
    }

    public void setType(int mType) {
        this.mType = mType;
    }

    public int getVer() {
        return mVer;
    }

    public void setVer(int mVer) {
        this.mVer = mVer;
    }

    public int getSize() {
        return mSize;
    }

    public void setSize(int mSize) {
        this.mSize = mSize;
    }

    public int getPri() {
        return mPri;
    }

    public void setPri(int mPri) {
        this.mPri = mPri;
    }

    public int getRespStat() {
        return mRespStat;
    }

    public void setRespStat(int mRespStat) {
        this.mRespStat = mRespStat;
    }

    public int getStat() {
        return mStat;
    }

    public void setStat(int mStat) {
        this.mStat = mStat;
    }

    public int getRetStat() {
        return mRetStat;
    }

    public void setRetStat(int mRetStat) {
        this.mRetStat = mRetStat;
    }

    public int getRetTxtCs() {
        return mRetTxtCs;
    }

    public void setRetTxtCs(int mRetTxtCs) {
        this.mRetTxtCs = mRetTxtCs;
    }

    public int getReadStat() {
        return mReadStat;
    }

    public void setReadStat(int mReadStat) {
        this.mReadStat = mReadStat;
    }

    public int getCtCls() {
        return mCtCls;
    }

    public void setCtCls(int mCtCls) {
        this.mCtCls = mCtCls;
    }

    public int getDelTime() {
        return mDelTime;
    }

    public void setDelTime(int mDelTime) {
        this.mDelTime = mDelTime;
    }

    public int getDelRpt() {
        return mDelRpt;
    }

    public void setDelRpt(int mDelRpt) {
        this.mDelRpt = mDelRpt;
    }

    public long getExp() {
        return mExp;
    }

    public void setExp(long mExp) {
        this.mExp = mExp;
    }

    public boolean isReadRpt() {
        return mReadRpt;
    }

    public void setReadRpt(boolean mReadRpt) {
        this.mReadRpt = mReadRpt;
    }

    public boolean isRptAllowed() {
        return mRptAllowed;
    }

    public void setRptAllowed(boolean mRptAllowed) {
        this.mRptAllowed = mRptAllowed;
    }

    public boolean isSeen() {
        return mSeen;
    }

    public void setSeen(boolean mSeen) {
        this.mSeen = mSeen;
    }

    public boolean isTxtOnl() {
        return mTxtOnl;
    }

    public void setTxtOnl(boolean mTxtOnl) {
        this.mTxtOnl = mTxtOnl;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getSub() {
        return mSub;
    }

    public void setSub(String mSub) {
        this.mSub = mSub;
    }

    public String getCtType() {
        return mCtType;
    }

    public void setCtType(String mCtType) {
        this.mCtType = mCtType;
    }

    public String getCls() {
        return mCls;
    }

    public void setCls(String mCls) {
        this.mCls = mCls;
    }

    public String getTranId() {
        return mTranId;
    }

    public void setTranId(String mTranId) {
        this.mTranId = mTranId;
    }

    public String getRetTxt() {
        return mRetTxt;
    }

    public void setRetTxt(String mRetTxt) {
        this.mRetTxt = mRetTxt;
    }

    public String getRespTxt() {
        return mRespTxt;
    }

    public void setRespTxt(String mRespTxt) {
        this.mRespTxt = mRespTxt;
    }

    /*
     * Message content (Picture/Text/etc)
     */

    private String mText;

    public void setText(String mText) {
        this.mText = mText;
    }

    public String getText() {
        return this.mText;
    }

    private String partId = "";

    public void setPartId(String partId) {
        this.partId = partId;
    }

    public String getPartId() {
        return this.partId;
    }
}
