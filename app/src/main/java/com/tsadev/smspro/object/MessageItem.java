package com.tsadev.smspro.object;

public abstract class MessageItem {

    public abstract void setId(long id);
    public abstract long getId();

    public abstract void setDate(long mDate);
    public abstract long getDate();

    public abstract void setDateSent(long mDateSent);
    public abstract long getDateSent();

    public abstract void setText(String mText);
    public abstract String getText();

    public abstract void setSubId(long mSubId);
    public abstract long getSubId();

    public abstract void setRead(boolean mRead);
    public abstract boolean isRead();

    public abstract void setLockStatus(boolean mLocked);
    public abstract boolean isLocked();

    public abstract String getSimpleTime();

}
