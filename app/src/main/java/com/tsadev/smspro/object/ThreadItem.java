package com.tsadev.smspro.object;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;

import com.tsadev.smspro.util.ContactHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ThreadItem implements Telephony.ThreadsColumns, Parcelable, Comparable<ThreadItem> {

    public static final String DEFAULT_SORT_ORDER = "date DESC";

    private long threadId;
    private long mDate;
    private int mMessageCount, mSnippetCharSet, mType, mError;
    private String displayName, number, normalNumber, simpleTime, mRecipientIds, mSnippet, lookupKey;
    private boolean isRead, isArchived, hasAttachments;
    private byte[] photoBytes;

    public ThreadItem() {
    }

    public void setThreadId(long threadId) {
        this.threadId = threadId;
    }

    public long getThreadId() {
        return this.threadId;
    }

    public void setDate(long date) {
        this.mDate = date;
        Date timestamp = new Date(date);
        if (DateUtils.isToday(date)) {
            DateFormat formatter = new SimpleDateFormat("h:mm a", Locale.US);
            this.simpleTime = formatter.format(timestamp);
        } else {
            DateFormat formatter = new SimpleDateFormat("MMM d", Locale.US);
            this.simpleTime = formatter.format(timestamp);
        }
    }

    public long getDate() {
        return this.mDate;
    }

    public void setMessageCount(int mMessageCount) {
        this.mMessageCount = mMessageCount;
    }

    int getMessageCount() {
        return this.mMessageCount;
    }

    public void setRecipientIds(String mRecipientIds) {
        this.mRecipientIds = mRecipientIds;
    }

    public String getRecipientIds() {
        return this.mRecipientIds;
    }

    public void setSnippet(String mSnippet) {
        this.mSnippet = mSnippet;
    }

    public String getSnippet() {
        return this.mSnippet;
    }

    public void setSnippetCharSet(int mSnippetCharSet) {
        this.mSnippetCharSet = mSnippetCharSet;
    }

    int getSnippetCharSet() {
        return this.mSnippetCharSet;
    }

    public void setRead(boolean isRead) {
        this.isRead = isRead;
    }

    boolean hasBeenRead() {
        return this.isRead;
    }

    public void setArchived(boolean isArchived) {
        this.isArchived = isArchived;
    }

    boolean isArchived() {
        return this.isArchived;
    }

    public void setType(int mType) {
        this.mType = mType;
    }

    int getType() {
        return this.mType;
    }

    public void setError(int mError) {
        this.mError = mError;
    }

    int getError() {
        return this.mError;
    }

    public void setHasAttachments(boolean hasAttachments) {
        this.hasAttachments = hasAttachments;
    }

    public boolean hasAttachments() {
        return this.hasAttachments;
    }

    public String getSimplifiedTime() {
        return this.simpleTime;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setNormalNumber(String normalNumber) {
        this.normalNumber = normalNumber;
    }

    public String getNormalNumber() {
        return this.normalNumber;
    }

    void setNumber(String number) {
        this.number = number;
    }

    String getNumber() {
        return this.number;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }

    public String getLookupKey() {
        return this.lookupKey;
    }

    public void setPhotoBytes(byte[] photoBytes) {
        this.photoBytes = photoBytes;
    }

    public byte[] getPhotoBytes() {
        return this.photoBytes;
    }


    public ThreadItem(Parcel in) {
        this.threadId = in.readLong();
        this.mDate = in.readLong();
        this.mMessageCount = in.readInt();
        this.mSnippetCharSet = in.readInt();
        this.mType = in.readInt();
        this.mError = in.readInt();
        this.displayName = in.readString();
        this.number = in.readString();
        this.normalNumber = in.readString();
        this.simpleTime = in.readString();
        this.mRecipientIds = in.readString();
        this.mSnippet = in.readString();
        this.lookupKey = in.readString();
        boolean[] bools = new boolean[3];
        in.readBooleanArray(bools);
        this.isRead = bools[0];
        this.isArchived = bools[1];
        this.hasAttachments = bools[2];
        in.readByteArray(this.photoBytes);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        boolean[] bools = new boolean[] {hasBeenRead(), isArchived(), hasAttachments()};
        dest.writeLong(getThreadId());
        dest.writeLong(getDate());
        dest.writeInt(getMessageCount());
        dest.writeInt(getSnippetCharSet());
        dest.writeInt(getType());
        dest.writeInt(getError());
        dest.writeString(getDisplayName());
        dest.writeString(getNumber());
        dest.writeString(getNormalNumber());
        dest.writeString(getSimplifiedTime());
        dest.writeString(getRecipientIds());
        dest.writeString(getSnippet());
        dest.writeString(getLookupKey());
        dest.writeBooleanArray(bools);
        dest.writeByteArray(getPhotoBytes());
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    public static final Parcelable.Creator<ThreadItem> CREATOR =
            new Parcelable.Creator<ThreadItem>() {
                public ThreadItem createFromParcel(Parcel in) {
                    return new ThreadItem(in);
                }

                public ThreadItem[] newArray(int size) {
                    return new ThreadItem[size];
                }
            };

    @Override
    public int compareTo(@NonNull ThreadItem another) {
        return Long.valueOf(mDate).compareTo(another.getDate());
    }


    /*

        String[] rIds = t.getRecipientIds().split("\\s+");
        if (rIds.length > 1) { //We have a group MMS
            getGroupMmsInfo(t, rIds);
        } else {
            if ((t.getSnippet() == null || t.getSnippet().isEmpty()) && t.hasAttachments()) getMmsSnippet(t);
            ContactHelper.Contact contact = ContactHelper.getContactFromThreadId(this, t.getThreadId());
            t.setDisplayName(contact.getDisplayName());
            t.setLookupKey(contact.getLookupKey());
            t.setNormalNumber(contact.getNormalizedNumber());
        }

        if (t.getLookupKey() != null && !t.getLookupKey().isEmpty()) getDisplayPicture(t);
     */
}
