package com.tsadev.smspro.object;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class MmsSmsItem implements Parcelable, Comparable<MmsSmsItem> {

    public MmsSmsItem() {

    }

    /*
     * Shared Variables
     */
    public String simpleTime, mText;
    public long id, mDate, mDateSent, mSubId;
    public boolean mRead, mLocked, mSeen;

    /*
     * SMS Variables
     */

    public int errCode, threadId, person, proto, status, type;

    public boolean replyPathPresent;

    public String addr, creator, svcCntr;

    /*
     * MMS Variables
     */

    public int mBox, mSubCs, mType, mVer,
            mSize, mPri, mRespStat, mStat,
            mRetStat, mRetTxtCs, mReadStat,
            mCtCls, mDelTime, mDelRpt;

    public long mExp;

    public boolean mReadRpt, mRptAllowed,
            mTxtOnl;

    public String mId, mSub, mCtType, mCls,
            mTranId, mRetTxt, mRespTxt, partId;

    public boolean mms;

    public volatile ArrayList<String> mmsPartIds = new ArrayList<>();


    public void setDate(long mDate) {
        this.mDate = mDate;
        Date timestamp = new Date(mDate);
        if (System.currentTimeMillis() - mDate < 86400000) {
            DateFormat formatter = new SimpleDateFormat("h:mm a", Locale.US);
            this.simpleTime = formatter.format(timestamp);
        } else {
            DateFormat formatter = new SimpleDateFormat("MMM d", Locale.US);
            this.simpleTime = formatter.format(timestamp);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.simpleTime);
        dest.writeString(this.mText);
        dest.writeLong(this.id);
        dest.writeLong(this.mDate);
        dest.writeLong(this.mDateSent);
        dest.writeLong(this.mSubId);
        dest.writeByte(this.mRead ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mLocked ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mSeen ? (byte) 1 : (byte) 0);
        dest.writeInt(this.errCode);
        dest.writeInt(this.threadId);
        dest.writeInt(this.person);
        dest.writeInt(this.proto);
        dest.writeInt(this.status);
        dest.writeInt(this.type);
        dest.writeByte(this.replyPathPresent ? (byte) 1 : (byte) 0);
        dest.writeString(this.addr);
        dest.writeString(this.creator);
        dest.writeString(this.svcCntr);
        dest.writeInt(this.mBox);
        dest.writeInt(this.mSubCs);
        dest.writeInt(this.mType);
        dest.writeInt(this.mVer);
        dest.writeInt(this.mSize);
        dest.writeInt(this.mPri);
        dest.writeInt(this.mRespStat);
        dest.writeInt(this.mStat);
        dest.writeInt(this.mRetStat);
        dest.writeInt(this.mRetTxtCs);
        dest.writeInt(this.mReadStat);
        dest.writeInt(this.mCtCls);
        dest.writeInt(this.mDelTime);
        dest.writeInt(this.mDelRpt);
        dest.writeLong(this.mExp);
        dest.writeByte(this.mReadRpt ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mRptAllowed ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mTxtOnl ? (byte) 1 : (byte) 0);
        dest.writeString(this.mId);
        dest.writeString(this.mSub);
        dest.writeString(this.mCtType);
        dest.writeString(this.mCls);
        dest.writeString(this.mTranId);
        dest.writeString(this.mRetTxt);
        dest.writeString(this.mRespTxt);
        dest.writeString(this.partId);
        dest.writeByte(this.mms ? (byte) 1 : (byte) 0);
        dest.writeStringList(this.mmsPartIds);
    }

    protected MmsSmsItem(Parcel in) {
        this.simpleTime = in.readString();
        this.mText = in.readString();
        this.id = in.readLong();
        this.mDate = in.readLong();
        this.mDateSent = in.readLong();
        this.mSubId = in.readLong();
        this.mRead = in.readByte() != 0;
        this.mLocked = in.readByte() != 0;
        this.mSeen = in.readByte() != 0;
        this.errCode = in.readInt();
        this.threadId = in.readInt();
        this.person = in.readInt();
        this.proto = in.readInt();
        this.status = in.readInt();
        this.type = in.readInt();
        this.replyPathPresent = in.readByte() != 0;
        this.addr = in.readString();
        this.creator = in.readString();
        this.svcCntr = in.readString();
        this.mBox = in.readInt();
        this.mSubCs = in.readInt();
        this.mType = in.readInt();
        this.mVer = in.readInt();
        this.mSize = in.readInt();
        this.mPri = in.readInt();
        this.mRespStat = in.readInt();
        this.mStat = in.readInt();
        this.mRetStat = in.readInt();
        this.mRetTxtCs = in.readInt();
        this.mReadStat = in.readInt();
        this.mCtCls = in.readInt();
        this.mDelTime = in.readInt();
        this.mDelRpt = in.readInt();
        this.mExp = in.readLong();
        this.mReadRpt = in.readByte() != 0;
        this.mRptAllowed = in.readByte() != 0;
        this.mTxtOnl = in.readByte() != 0;
        this.mId = in.readString();
        this.mSub = in.readString();
        this.mCtType = in.readString();
        this.mCls = in.readString();
        this.mTranId = in.readString();
        this.mRetTxt = in.readString();
        this.mRespTxt = in.readString();
        this.partId = in.readString();
        this.mms = in.readByte() != 0;
        this.mmsPartIds = in.createStringArrayList();
    }

    public static final Creator<MmsSmsItem> CREATOR = new Creator<MmsSmsItem>() {
        @Override
        public MmsSmsItem createFromParcel(Parcel source) {
            return new MmsSmsItem(source);
        }

        @Override
        public MmsSmsItem[] newArray(int size) {
            return new MmsSmsItem[size];
        }
    };

    @Override
    public int compareTo(@NonNull MmsSmsItem another) {
        return Long.valueOf(mDate).compareTo(another.mDate);
    }
}
